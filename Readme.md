# **DiscussIT - discussion forum engine**

#### **1. Overview**

DiscussIT is fully operational discussion forum engine. After successful sign in You can start creating discussions and writing posts. You are able also to send private messages to other users.

#### **2. Goal**

Main goal for creating this project was to show my abilities to create enterprise software applications with stack: Java8, Spring framework, JPA, Hibernate, MySQL<br>
Taking this as a main goal, the focus was put on backend programming.

#### **3. Author**

You can find more information about me on my website (www.grzegorznowosad.pl)

#### **4. Technology stack**
Some of the main technologies that were used:

Backend:
- Java Core
- Java 8
- Spring framework (Boot, Core, MVC, Security)
- JPA
- Hibernate
- MySQL

Frontend:
- Thymeleaf
- HTML
- Bootstrap 4
- CSS


and additionally some of every-day tools like

- IntelliJ
- Maven
- Git

#### **5. Testing**

For test purposes tests using JUnit 4 and Mockito has been written. Tests are available in specific directory (src/test/java/pl/grzegorznowosad/DiscussIT).
Unit tests were written to check (test) if application functions works correctly.


#### **6. How to use DiscussIT**

- login in, registering

First what you need to do is to sign in to DiscussIT. For not logged users only the welcome (main page) is available.
To sign in into the service use Your credentials or register for Your own new account.

There have been already created 2 accounts for Your convenience. You can use them to explore world of movies on Watch!T.


```
user:   john
password: john123
role: user
```

or

```
user:   lucy
password: lucy123
role: admin
```

- discussions, messages

You are able to create discussions, write messages in other discussions

- private messages

You are able to send private messages to other users. Those messages are seen only by sender and receiver.

- profile edit

You are able to edit Your profile

- admin panel

If Your role in forum is admin or GOD, You have access to admin panel, where You can find possibilities to:

    - delete user
    - change role for user
    

Thank You for spending time using DiscussIT. I really appreciate feedback so please do not hestitate to share with me Your thoughts and suggestions.