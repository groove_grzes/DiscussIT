package pl.grzegorznowosad.DiscussIT.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateUpdateCategoryDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.CategoryDto;
import pl.grzegorznowosad.DiscussIT.service.CategoryService;
import pl.grzegorznowosad.DiscussIT.service.exception.CategoryAlreadyExist;
import pl.grzegorznowosad.DiscussIT.service.exception.CategoryIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.CategoryNotFound;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class  CategoryController {

    @Autowired
    private CategoryService categoryService;

    // getAll
    @GetMapping("category")
    public List<CategoryDto> getAll(){
        return categoryService.getAll();
    }

    // getById
    @GetMapping("category/{id}")
    public CategoryDto getById(@PathVariable Integer id) throws CategoryNotFound {
        return categoryService.getById(id);
    }

    // create
    @PostMapping("category")
    public CategoryDto createCategory(@RequestBody CreateUpdateCategoryDto createUpdateCategoryDto) throws CategoryIncorrect, CategoryAlreadyExist {
        return categoryService.createCategory(createUpdateCategoryDto);
    }

    // update
    @PutMapping("category/{id}")
    public CategoryDto updateCategory(@RequestBody CreateUpdateCategoryDto createUpdateCategoryDto, @PathVariable Integer id) throws CategoryNotFound, CategoryIncorrect {
        return categoryService.updateCategory(id, createUpdateCategoryDto);
    }

    // delete
    @DeleteMapping("category/{id}")
    public CategoryDto deleteCategory(@PathVariable Integer id) throws CategoryNotFound {
        return categoryService.deleteCategory(id);
    }
}
