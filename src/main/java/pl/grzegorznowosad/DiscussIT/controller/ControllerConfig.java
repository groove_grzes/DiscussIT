package pl.grzegorznowosad.DiscussIT.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import pl.grzegorznowosad.DiscussIT.service.exception.*;

@ControllerAdvice
public class ControllerConfig {

    @ExceptionHandler({UserAlreadyExist.class, LoginIncorrect.class, UserNotFound.class, CategoryAlreadyExist.class, CategoryIncorrect.class, CategoryNotFound.class,
            SectionAlreadyExist.class, SectionIncorrect.class, SectionNotFound.class,DiscussionAlreadyExist.class, DiscussionIncorrect.class, DiscussionNotFound.class,
            MessageIncorrect.class, MessageNotFound.class, MessageAlreadyExist.class, ShoutboxMessageNotFound.class, ShoutboxMessageIncorrect.class, PrivateMessageDataIncorrect.class,
            PrivateMessageNotFound.class, RoleApplicationNotFound.class, RoleApplicationAlreadyExist.class, RoleApplicationDataIncorrect.class})
//    @ResponseStatus(code = HttpStatus.CONFLICT, reason = "User already exist")
    public ModelAndView handleExceptions(Exception e){
        ModelAndView mav = new ModelAndView("oops");

            mav.addObject("errorCode", e.getClass().getSimpleName());

        return mav;
    }

//    @ExceptionHandler(UserNotFound.class)
//    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "User not found")
//    public void handleUserNotFound(){
//
//    }

//    @ExceptionHandler(CategoryAlreadyExist.class)
//    @ResponseStatus(code = HttpStatus.CONFLICT, reason = "Category already exist")
//    public void handleCategoryAlreadyExist(){
//
//    }

//    @ExceptionHandler(CategoryIncorrect.class)
//    @ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Category incorrect")
//    public void handleCategoryIncorrect(){
//
//    }

//    @ExceptionHandler(CategoryNotFound.class)
//    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Category not found")
//    public void handleCategoryNotFound(){
//
//    }

//    @ExceptionHandler(SectionAlreadyExist.class)
//    @ResponseStatus(code = HttpStatus.CONFLICT, reason = "Section already exist")
//    public void handleSectionAlreadyExist(){
//
//    }

//    @ExceptionHandler(SectionIncorrect.class)
//    @ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Section incorrect")
//    public void handleSectionIncorrect(){
//
//    }

//    @ExceptionHandler(SectionNotFound.class)
//    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Section not found")
//    public void handleSectionNotFound(){
//
//    }

//    @ExceptionHandler(DiscussionAlreadyExist.class)
//    @ResponseStatus(code = HttpStatus.CONFLICT, reason = "Discussion already exist")
//    public void handleDiscussionAlreadyExist(){
//
//    }

//    @ExceptionHandler(DiscussionIncorrect.class)
//    @ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Discussion data incorrect")
//    public void handleDiscussionIncorrect(){
//
//    }

//    @ExceptionHandler(DiscussionNotFound.class)
//    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Discussion not found")
//    public void handleDiscussionNotFound(){
//
//    }

//    @ExceptionHandler(MessageIncorrect.class)
//    @ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Message data incorrect")
//    public void handleMessageIncorrect(){
//
//    }

//    @ExceptionHandler(MessageNotFound.class)
//    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Message not found")
//    public void handleMessageNotFound(){
//
//    }

//    @ExceptionHandler(MessageAlreadyExist.class)
//    @ResponseStatus(code = HttpStatus.CONFLICT, reason = "Message already exist")
//    public void handleMessageAlreadyExist(){
//
//    }

//    @ExceptionHandler(ShoutboxMessageNotFound.class)
//    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Shoutbox Message not found")
//    public void handleShoutboxMessageNotFound(){
//
//    }

//    @ExceptionHandler(ShoutboxMessageIncorrect.class)
//    @ResponseStatus(code = HttpStatus.CONFLICT, reason = "Shoutbox Message data incorrect, text aleady exist")
//    public void handleShoutboxMessageIncorrect(){
//
//    }

//    @ExceptionHandler(PrivateMessageDataIncorrect.class)
//    @ResponseStatus(code = HttpStatus.CONFLICT, reason = "Private Message data incorrect")
//    public void handlePrivateMessageDataIncorrect(){
//
//    }

//    @ExceptionHandler(PrivateMessageNotFound.class)
//    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Private Message not found")
//    public void handlePrivateMessageNotFound(){
//
//    }

//    @ExceptionHandler(RoleApplicationNotFound.class)
//    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Role not found")
//    public void handleRoleApplicationNotFound(){
//
//    }

//    @ExceptionHandler(RoleApplicationAlreadyExist.class)
//    @ResponseStatus(code = HttpStatus.CONFLICT, reason = "Role already exist")
//    public void handleRoleApplicationAlreadyExist(){
//
//    }

//    @ExceptionHandler(RoleApplicationDataIncorrect.class)
//    @ResponseStatus(code = HttpStatus.CONFLICT, reason = "Role data incorrect")
//    public void handleRoleApplicationDataIncorrect(){
//
//    }

}
