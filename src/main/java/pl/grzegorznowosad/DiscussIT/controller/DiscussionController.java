package pl.grzegorznowosad.DiscussIT.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateDiscussionDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.DiscussionDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.UpdateDiscussionDto;
import pl.grzegorznowosad.DiscussIT.service.DiscussionService;
import pl.grzegorznowosad.DiscussIT.service.exception.DiscussionAlreadyExist;
import pl.grzegorznowosad.DiscussIT.service.exception.DiscussionIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.DiscussionNotFound;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class DiscussionController {

    @Autowired
    private DiscussionService discussionService;

    // getAll
    @GetMapping("discussion")
    public List<DiscussionDto> getAll(){
        return discussionService.getAll();
    }

    // getById
    @GetMapping("discussion/{id}")
    public DiscussionDto getById(@PathVariable Integer id) throws DiscussionNotFound {
        return discussionService.getById(id);
    }

    // getByTopic
    // narazie bez implemenetacji kontrollera http

    // create
    @PostMapping("discussion")
    public DiscussionDto createDiscussion(@RequestBody CreateDiscussionDto createDiscussionDto) throws DiscussionNotFound, DiscussionAlreadyExist, DiscussionIncorrect {
        return discussionService.createDiscussion(createDiscussionDto);
    }

    // update
    @PutMapping("discussion/{id}")
    public DiscussionDto updateDiscussion(@PathVariable Integer id, @RequestBody UpdateDiscussionDto updateDiscussionDto) throws DiscussionIncorrect {
        return discussionService.updateDiscussion(updateDiscussionDto, id);
    }

    // delete
    @DeleteMapping("discussion/{id}")
    public DiscussionDto deleteDiscussion(@PathVariable Integer id) throws DiscussionNotFound {
        return discussionService.deleteDiscussion(id);
    }
}
