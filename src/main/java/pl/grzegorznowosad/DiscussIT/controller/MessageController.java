package pl.grzegorznowosad.DiscussIT.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateMessageDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.MessageDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.UpdateMessageDto;
import pl.grzegorznowosad.DiscussIT.service.MessageService;
import pl.grzegorznowosad.DiscussIT.service.exception.MessageAlreadyExist;
import pl.grzegorznowosad.DiscussIT.service.exception.MessageIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.MessageNotFound;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class MessageController {

    @Autowired
    private MessageService messageService;

    // getAll
    @GetMapping("message")
    public List<MessageDto> getAll(){
        return messageService.getAll();
    }

    // getById
    @GetMapping("message/{id}")
    public MessageDto getById(@PathVariable Integer id) throws MessageNotFound {
        return messageService.getById(id);
    }

    // getByText
    // narazie bez implementacji controllera http

    // create
    @PostMapping("message")
    public MessageDto createMessage(@RequestBody CreateMessageDto createMessageDto) throws MessageIncorrect, MessageAlreadyExist {
        return messageService.createMessage(createMessageDto);
    }

    // update
    @PutMapping("message/{id}")
    public MessageDto updateMessage(@RequestBody UpdateMessageDto updateMessageDto, @PathVariable Integer id) throws MessageIncorrect, MessageNotFound {
        return messageService.updateMessage(updateMessageDto, id);
    }

    // delete
    @DeleteMapping("message/{id}")
    public MessageDto deleteMessage(@PathVariable Integer id) throws MessageNotFound {
        return messageService.deleteMessage(id);
    }
}
