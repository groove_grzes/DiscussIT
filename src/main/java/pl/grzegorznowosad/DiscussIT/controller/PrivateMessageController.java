package pl.grzegorznowosad.DiscussIT.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreatePrivateMessageDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.PrivateMessageDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.UpdatePrivateMessageDto;
import pl.grzegorznowosad.DiscussIT.service.PrivateMessageService;
import pl.grzegorznowosad.DiscussIT.service.exception.PrivateMessageDataIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.PrivateMessageNotFound;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class PrivateMessageController {

    @Autowired
    private PrivateMessageService privateMessageService;

    // getAll
    @GetMapping("privateMessage")
    public List<PrivateMessageDto> getAll(){
        return privateMessageService.getAll();
    }

    // getById
    @GetMapping("privateMessage/{id}")
    public PrivateMessageDto getById(@PathVariable Integer id) throws PrivateMessageNotFound {
        return privateMessageService.getById(id);
    }

    // create
    @PostMapping("privateMessage")
    public PrivateMessageDto create(@RequestBody CreatePrivateMessageDto createPrivateMessageDto) throws UserNotFound, PrivateMessageDataIncorrect {
        return privateMessageService.create(createPrivateMessageDto);
    }

    // update
    @PutMapping("privateMesssge/{id}")
    public PrivateMessageDto update(@RequestBody UpdatePrivateMessageDto updatePrivateMessageDto, @PathVariable Integer id) throws UserNotFound, PrivateMessageNotFound {
        return privateMessageService.update(updatePrivateMessageDto, id);
    }

    // delete
    @DeleteMapping("privateMessage/{id}")
    public PrivateMessageDto delete(@PathVariable Integer id) throws PrivateMessageNotFound {
        return privateMessageService.delete(id);
    }

    // after reading message (change isNew to "no"
    @PutMapping("privateMessage/edit/{id}")
    public PrivateMessageDto readMessage(@PathVariable Integer id) throws PrivateMessageDataIncorrect {
        return privateMessageService.readMessage(id);
    }
}
