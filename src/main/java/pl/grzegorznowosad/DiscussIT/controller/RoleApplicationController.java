package pl.grzegorznowosad.DiscussIT.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateUpdateRoleApplication;
import pl.grzegorznowosad.DiscussIT.controller.dto.RoleApplicationDto;
import pl.grzegorznowosad.DiscussIT.service.RoleApplicationService;
import pl.grzegorznowosad.DiscussIT.service.exception.RoleApplicationAlreadyExist;
import pl.grzegorznowosad.DiscussIT.service.exception.RoleApplicationDataIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.RoleApplicationNotFound;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class RoleApplicationController {

    @Autowired
    private RoleApplicationService roleApplicationService;

    // getAll
    @GetMapping("roles")
    public List<RoleApplicationDto> getAll(){
        return roleApplicationService.getAll();
    }

    // getById
    @GetMapping("roles/{id}")
    public RoleApplicationDto getById(@PathVariable Integer id) throws RoleApplicationNotFound {
        return roleApplicationService.getById(id);
    }

    // getByRoleName
    public RoleApplicationDto getByRoleName(String roleName) throws RoleApplicationNotFound {
        return roleApplicationService.getByRoleName(roleName);
    }

    // create
    @PostMapping("roles")
    public RoleApplicationDto create(@RequestBody CreateUpdateRoleApplication createUpdateRoleApplication) throws RoleApplicationDataIncorrect, RoleApplicationAlreadyExist {
        return roleApplicationService.create(createUpdateRoleApplication);
    }

    // update
    @PutMapping("roles/{id}")
    public RoleApplicationDto update(@RequestBody CreateUpdateRoleApplication createUpdateRoleApplication, @PathVariable Integer id) throws RoleApplicationDataIncorrect {
        return roleApplicationService.update(id, createUpdateRoleApplication);
    }

    // delete
    @DeleteMapping("roles/{id}")
    public RoleApplicationDto delete(@PathVariable Integer id) throws RoleApplicationAlreadyExist {
        return roleApplicationService.delete(id);
    }
}
