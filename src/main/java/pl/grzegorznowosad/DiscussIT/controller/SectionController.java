package pl.grzegorznowosad.DiscussIT.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateUpdateSectionDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.SectionDto;
import pl.grzegorznowosad.DiscussIT.service.SectionService;
import pl.grzegorznowosad.DiscussIT.service.exception.CategoryNotFound;
import pl.grzegorznowosad.DiscussIT.service.exception.SectionAlreadyExist;
import pl.grzegorznowosad.DiscussIT.service.exception.SectionIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.SectionNotFound;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class SectionController {

    @Autowired
    private SectionService sectionService;

    // getAll
    @GetMapping("section")
    public List<SectionDto> getAll(){
        return sectionService.getAll();
    }

    // getById
    @GetMapping("section/{id}")
    public SectionDto getById(@PathVariable Integer id) throws SectionNotFound {
        return sectionService.getById(id);
    }

    // getBySectionTopic
    // narazie bez controllera http

    // create
    @PostMapping("section")
    public SectionDto create(@RequestBody CreateUpdateSectionDto createUpdateSectionDto) throws SectionAlreadyExist, SectionIncorrect, CategoryNotFound {
        return sectionService.createSection(createUpdateSectionDto);
    }

    // update
    @PutMapping("section/{id}")
    public SectionDto update(@PathVariable Integer id, @RequestBody CreateUpdateSectionDto createUpdateSectionDto) throws SectionNotFound, SectionIncorrect {
        return sectionService.updateSection(id, createUpdateSectionDto);
    }

    // delete
    @DeleteMapping("section/{id}")
    public SectionDto delete(@PathVariable Integer id) throws SectionNotFound {
        return sectionService.deleteSection(id);
    }
}
