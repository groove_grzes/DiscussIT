package pl.grzegorznowosad.DiscussIT.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateUpdateShoutboxMessageDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.ShoutboxMessageDto;
import pl.grzegorznowosad.DiscussIT.model.ShoutboxMessage;
import pl.grzegorznowosad.DiscussIT.service.ShoutboxService;
import pl.grzegorznowosad.DiscussIT.service.exception.ShoutboxMessageIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.ShoutboxMessageNotFound;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class ShoutboxController {

    @Autowired
    private ShoutboxService shoutboxService;

    // getAll
    @GetMapping("shoutbox")
    public List<ShoutboxMessageDto> getAll(){
        return shoutboxService.getAll();
    }

    // getById
    @GetMapping("shoutbox/{id}")
    public ShoutboxMessageDto getById(@PathVariable Integer id) throws ShoutboxMessageNotFound {
        return shoutboxService.getById(id);
    }

    // getByText
//    @GetMapping("shoutbox/{text}")
//    public ShoutboxMessageDto getByText(@PathVariable String text) throws ShoutboxMessageNotFound {
//        return shoutboxService.getByText(text);
//    }

    // create
    @PostMapping("shoutbox")
    public ShoutboxMessageDto create(@RequestBody CreateUpdateShoutboxMessageDto createUpdateShoutboxMessageDto) throws UserNotFound, ShoutboxMessageIncorrect {
        return shoutboxService.create(createUpdateShoutboxMessageDto);
    }

    // update
    @PutMapping("shoutbox/{id}")
    public ShoutboxMessageDto update(@PathVariable Integer id, @RequestBody CreateUpdateShoutboxMessageDto createUpdateShoutboxMessageDto) throws ShoutboxMessageNotFound {
        return shoutboxService.update(createUpdateShoutboxMessageDto, id);
    }

    // delete
    @DeleteMapping("shoutbox/{id}")
    public ShoutboxMessageDto delete(@PathVariable Integer id) throws ShoutboxMessageNotFound {
        return shoutboxService.delete(id);
    }
}
