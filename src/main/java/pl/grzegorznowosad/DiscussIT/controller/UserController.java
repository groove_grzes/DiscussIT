package pl.grzegorznowosad.DiscussIT.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.DiscussIT.controller.dto.UpdateUserDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.UserCreateDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.UserDto;
import pl.grzegorznowosad.DiscussIT.service.UserService;
import pl.grzegorznowosad.DiscussIT.service.exception.LoginIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.RoleApplicationNotFound;
import pl.grzegorznowosad.DiscussIT.service.exception.UserAlreadyExist;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;

import java.util.List;

@Controller
@RestController
@RequestMapping("/api/v1/")
public class UserController {

    @Autowired
    private UserService userService;

    // getAll
    @GetMapping("user")
    public List<UserDto> getAll(){
        return userService.getAll();
    }

    // getByID
    @GetMapping("user/{id}")
    public UserDto getById(@PathVariable Integer id) throws UserNotFound {
        return userService.getById(id);
    }

    // getAllByLogin
    // bez kontrolera narazie

    // addUser
    @PostMapping("user/")
    public UserDto addUser(@RequestBody UserCreateDto createDto) throws LoginIncorrect, UserAlreadyExist, RoleApplicationNotFound {
        return userService.createUser(createDto);

    }

    // update
    @PutMapping("user/{id}")
    public UserDto updateUser(@PathVariable Integer id, @RequestBody UpdateUserDto updateUserDto) throws UserNotFound {
        return userService.updateUser(id, updateUserDto);
    }

    // delete
    @DeleteMapping("user/{id}")
    public UserDto deleteUser(@PathVariable Integer id) throws UserNotFound {
        return userService.deleteUser(id);
    }
}
