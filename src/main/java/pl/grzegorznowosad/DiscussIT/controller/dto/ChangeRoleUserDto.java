package pl.grzegorznowosad.DiscussIT.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChangeRoleUserDto {

    private String userLogin;
    private String newUserRole;
}
