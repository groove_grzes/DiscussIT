package pl.grzegorznowosad.DiscussIT.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateDiscussionViewDto {

    @NotBlank
    private String topic;

    @NotBlank
    private String textMessage;
    private String userLogin;
    private Integer sectionId;


}
