package pl.grzegorznowosad.DiscussIT.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreatePrivateMessageViewDto {

    @NotBlank
    private String text;
    private Integer senderId;
    private String senderLogin;
    private Integer receiverId;
    private String receiverLogin;

}
