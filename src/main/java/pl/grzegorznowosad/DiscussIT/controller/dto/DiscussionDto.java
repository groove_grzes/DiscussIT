package pl.grzegorznowosad.DiscussIT.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class DiscussionDto {

    private Integer id;
    private String date;
    private String topic;

    private Integer userId;
    private Integer sectionId;
    private String userLogin;

}
