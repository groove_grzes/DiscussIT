package pl.grzegorznowosad.DiscussIT.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiscussionViewDto {

    private Integer id;
    private String date;
    private String topic;
    private String userLogin;
    private Integer userId;
    private Integer sectionId;
}
