package pl.grzegorznowosad.DiscussIT.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageDto {

    private Integer id;
    private String text;
    private String date;
    private Integer userId;
    private Integer discussionId;

}
