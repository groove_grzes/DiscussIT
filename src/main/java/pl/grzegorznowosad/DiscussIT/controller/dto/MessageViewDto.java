package pl.grzegorznowosad.DiscussIT.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageViewDto {

    private Integer id;
    private String text;
    private String date;
    private String userLogin;
    private Integer userId;
    private Integer discussionId;
    private String role;

    private String avatar;

}
