package pl.grzegorznowosad.DiscussIT.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PrivateMessageDto {

    private Integer id;
    private String date;
    private String text;
    private String isNew;
    private Integer senderId;
    private Integer receiverId;

}
