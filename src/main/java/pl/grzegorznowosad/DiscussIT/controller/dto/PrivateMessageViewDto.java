package pl.grzegorznowosad.DiscussIT.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PrivateMessageViewDto{
    private Integer id;
    private String date;
    private String text;
    private String isNew;
    private Integer senderId;
    private String senderLogin;
    private Integer receiverId;
    private String receiverLogin;
}