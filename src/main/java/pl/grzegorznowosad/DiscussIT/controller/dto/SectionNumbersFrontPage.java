package pl.grzegorznowosad.DiscussIT.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SectionNumbersFrontPage {

    private Integer numberOfDiscussions;
    private Integer numberOfMessages;
    private String authorOfLastPost;
    private Integer authorOfLastPostId;
    private String date;
    private String userAvatar;

}
