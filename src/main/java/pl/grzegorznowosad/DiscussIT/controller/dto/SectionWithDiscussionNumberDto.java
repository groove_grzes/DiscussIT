package pl.grzegorznowosad.DiscussIT.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SectionWithDiscussionNumberDto {

    private Integer sectionId;
    private Integer discussionNumber;

}
