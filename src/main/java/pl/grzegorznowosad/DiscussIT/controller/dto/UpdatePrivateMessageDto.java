package pl.grzegorznowosad.DiscussIT.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdatePrivateMessageDto {

    private String text;
    private Integer senderId;
    private Integer receiverId;
    private String isNew;

}
