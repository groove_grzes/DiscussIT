package pl.grzegorznowosad.DiscussIT.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Blob;
import java.sql.SQLException;

public class UpdateUserDto {

    private String password;
    private String firstName;
    private Integer birthYear;
    private String email;
    private Blob avatar;

    public UpdateUserDto() {
    }

    public UpdateUserDto(String password, String firstName, Integer birthYear, String email, Blob avatar) {
        this.password = password;
        this.firstName = firstName;
        this.birthYear = birthYear;
        this.email = email;
        this.avatar = avatar;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(Integer birthYear) {
        this.birthYear = birthYear;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Blob getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] file) throws SQLException {
        this.avatar = new SerialBlob(file);
    }
}
