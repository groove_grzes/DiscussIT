package pl.grzegorznowosad.DiscussIT.controller.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateDtoWithFile {

    @NotBlank
    @Size(min = 2)
    private String login;

    @NotBlank
    @Size(min = 5)
    private String password;

    @NotBlank
    @Size(min = 3)
    private String firstName;

    @NotNull
    @Min(1900)
    @Max(2018)
    private Integer birthYear;

    @NotBlank(message = "email can not be empty")
    @Email
    private String email;

    @NotNull(message = "pls choose file as avatar")
    private MultipartFile avatar;

}
