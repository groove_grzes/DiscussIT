package pl.grzegorznowosad.DiscussIT.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.tomcat.util.codec.binary.Base64;

import java.sql.Blob;
import java.sql.SQLException;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private Integer id;
    private String login;
    private String firstName;
    private Integer birthYear;
    private String email;
    private Blob avatar;
    private String isActive;
    private String role;

    public String getImageAsString() throws SQLException {
        int blobLength = (int) avatar.length();
        byte[] blobAsBytes = avatar.getBytes(1, blobLength);

        String obrazekString = Base64.encodeBase64String(blobAsBytes);

        return obrazekString;
    }
}
