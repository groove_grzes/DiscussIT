package pl.grzegorznowosad.DiscussIT.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Blob;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String login;
    private String password;
    private String firstName;
    private Integer birthYear;
    private String email;
    private String isActive;

    @Lob
    @Column(name="AVATAR_IMAGE", nullable=false, columnDefinition="mediumblob")
    private Blob avatar;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Discussion> allDiscussions;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private RoleApplication role;
}
