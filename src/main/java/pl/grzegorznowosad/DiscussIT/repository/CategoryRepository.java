package pl.grzegorznowosad.DiscussIT.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.grzegorznowosad.DiscussIT.model.Category;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Integer> {
    List<Category> findAll();
    Optional<Category> findById(Integer id);
    Optional<Category> findByCategoryTopic(String topic);
}
