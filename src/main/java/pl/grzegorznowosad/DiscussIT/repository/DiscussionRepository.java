package pl.grzegorznowosad.DiscussIT.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.grzegorznowosad.DiscussIT.model.Discussion;
import pl.grzegorznowosad.DiscussIT.model.Section;
import pl.grzegorznowosad.DiscussIT.model.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface DiscussionRepository extends CrudRepository<Discussion, Integer> {

    List<Discussion> findAll();
    Optional<Discussion> findByTopic(String topic);
    Optional<Discussion> findBySection(Integer id);

    List<Discussion> findAllBySection(Section section);
    List<Discussion> findTop5ByOrderByIdDesc();

    List<Discussion> findAllByUser(User user);
}
