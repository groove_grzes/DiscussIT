package pl.grzegorznowosad.DiscussIT.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.grzegorznowosad.DiscussIT.model.Discussion;
import pl.grzegorznowosad.DiscussIT.model.Message;
import pl.grzegorznowosad.DiscussIT.model.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface MessageRepository extends CrudRepository<Message, Integer> {

    List<Message> findAll();
    Optional<Message> findById(Integer id);
    Optional<Message> findByText(String text);
    List<Message> findAllByUser(User user);

    List<Message> findAllByDiscussion(Discussion discussion);
    List<Message> findTop10ByOrderByIdDesc();

}
