package pl.grzegorznowosad.DiscussIT.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.grzegorznowosad.DiscussIT.model.PrivateMessage;
import pl.grzegorznowosad.DiscussIT.model.User;

import java.util.List;

@Repository
public interface PrivateMessageRepository extends CrudRepository<PrivateMessage, Integer> {

    List<PrivateMessage> findAll();
    List<PrivateMessage> findAllByReceiverOrderByIdDesc(User receiver);
    List<PrivateMessage> findAllBySenderOrderByIdDesc(User sender);
    List<PrivateMessage> findAllByReceiverAndIsNewOrderByIdDesc(User user, String isMessageNew);

}
