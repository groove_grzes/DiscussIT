package pl.grzegorznowosad.DiscussIT.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.grzegorznowosad.DiscussIT.model.RoleApplication;

import javax.management.relation.Role;
import java.util.List;
import java.util.Optional;

@Repository
public interface RoleApplicationRepository extends CrudRepository<RoleApplication, Integer> {

    List<RoleApplication> findAll();
    Optional<RoleApplication> findById(Integer id);
    Optional<RoleApplication> findByRoleName(String roleName);

}
