package pl.grzegorznowosad.DiscussIT.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.grzegorznowosad.DiscussIT.model.Category;
import pl.grzegorznowosad.DiscussIT.model.Section;

import java.util.List;
import java.util.Optional;

@Repository
public interface SectionRepository extends CrudRepository<Section, Integer> {
    List<Section> findAll();
    Optional<Section> findBySectionTopic(String topic);

    List<Section> findByCategory(Category category);

}
