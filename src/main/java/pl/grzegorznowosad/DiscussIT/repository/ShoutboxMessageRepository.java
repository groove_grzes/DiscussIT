package pl.grzegorznowosad.DiscussIT.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.grzegorznowosad.DiscussIT.model.ShoutboxMessage;

import java.util.List;
import java.util.Optional;

@Repository
public interface ShoutboxMessageRepository extends CrudRepository<ShoutboxMessage, Integer> {
    List<ShoutboxMessage> findAll();
    Optional<ShoutboxMessage> findByText(String text);

    List<ShoutboxMessage> findTop10ByOrderByIdDesc();

}
