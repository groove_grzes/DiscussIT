package pl.grzegorznowosad.DiscussIT.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;

import java.util.Optional;

@Service
public class MyUserDetails implements UserDetailsService{

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userLogin) throws UsernameNotFoundException {
        Optional<User> userByLogin = userRepository.findByLogin(userLogin);

        if(userByLogin.isPresent()){
            User user = userByLogin.get();

            return org.springframework.security.core.userdetails.User
                    .withUsername(user.getLogin())
                    .password(user.getPassword())
                    .authorities("ROLE_" + user.getRole().getRoleName())
                    .build();

        }else{
            throw new UsernameNotFoundException("invalid login, user not found");
        }
    }
}
