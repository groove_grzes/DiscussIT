package pl.grzegorznowosad.DiscussIT.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private MyUserDetails myUserDetails;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception{
        DaoAuthenticationProvider dap = new DaoAuthenticationProvider();

        dap.setUserDetailsService(myUserDetails);
        dap.setPasswordEncoder(new BCryptPasswordEncoder());

        auth.authenticationProvider(dap);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers(HttpMethod.GET,"/bootstrap/**").permitAll()
                .antMatchers(HttpMethod.GET,"/marina_teal/**").permitAll()
                .antMatchers(HttpMethod.GET,"/home2.css").permitAll()
                .antMatchers(HttpMethod.GET,"/other/**").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers("/faq").permitAll()
                .antMatchers("/register").permitAll()
                .antMatchers("/home2").permitAll()
//                .antMatchers("/section2.html").permitAll()
//                .antMatchers("/users").permitAll()
                .antMatchers("/admin/**").hasAnyRole("ADMIN", "GOD")
//                .antMatchers("/godPanel/**").hasRole("GOD")
                .anyRequest().authenticated() // wszystkie pozostałe zapytania dostępne tylko dla zalogowanych
                .and()
                .formLogin()
                .loginPage("/signin.html")
                .usernameParameter("username")
                .passwordParameter("password")
//                .successForwardUrl("/home")
//                .defaultSuccessUrl("/")
//                .failureForwardUrl("/loginpage.html")
                .permitAll()
                .and()
                .logout()
                .logoutSuccessUrl("/")
                .and()
                .exceptionHandling().accessDeniedPage("/accessDenied");
    }
}
