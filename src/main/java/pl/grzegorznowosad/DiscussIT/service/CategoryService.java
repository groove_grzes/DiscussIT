package pl.grzegorznowosad.DiscussIT.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateUpdateCategoryDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.CategoryDto;
import pl.grzegorznowosad.DiscussIT.repository.CategoryRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.CategoryAlreadyExist;
import pl.grzegorznowosad.DiscussIT.service.exception.CategoryIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.CategoryNotFound;
import pl.grzegorznowosad.DiscussIT.service.mapper.CategoryDtoMapper;
import pl.grzegorznowosad.DiscussIT.model.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CategoryDtoMapper categoryDtoMapper;

    // getAll
    public List<CategoryDto> getAll(){
        List<Category> all = categoryRepository.findAll();
        List<CategoryDto> allDtos = new ArrayList<>();

        all.forEach(x -> {
            allDtos.add(categoryDtoMapper.toDto(x));
        });

        return allDtos;
    }

    // getByID
    public CategoryDto getById(Integer id) throws CategoryNotFound {
        Optional<Category> byId = categoryRepository.findById(id);
        if(byId.isPresent()){
            Category category = byId.get();
            return categoryDtoMapper.toDto(category);
        }else{
            throw new CategoryNotFound();
        }
    }

    // getByTopic
    public CategoryDto getByTopic(String topic) throws CategoryAlreadyExist {
        Optional<Category> byCategoryTopic = categoryRepository.findByCategoryTopic(topic);
        if(byCategoryTopic.isPresent()){
            Category category = byCategoryTopic.get();
            return categoryDtoMapper.toDto(category);
        }else{
            throw new CategoryAlreadyExist();
        }
    }

    // create
    public CategoryDto createCategory(CreateUpdateCategoryDto createUpdateCategoryDto) throws CategoryIncorrect, CategoryAlreadyExist {
        if(createUpdateCategoryDto.getCategoryTopic() == null || createUpdateCategoryDto.getCategoryTopic().isEmpty()){
            throw new CategoryIncorrect();
        }

        if(categoryRepository.findByCategoryTopic(createUpdateCategoryDto.getCategoryTopic()).isPresent()){
            throw new CategoryAlreadyExist();
        }

        Category savedCategory = categoryRepository.save(categoryDtoMapper.toModel(createUpdateCategoryDto));
        return categoryDtoMapper.toDto(savedCategory);

    }

    // update
    public CategoryDto updateCategory(Integer id, CreateUpdateCategoryDto createUpdateCategoryDto) throws CategoryIncorrect, CategoryNotFound {
        if(createUpdateCategoryDto.getCategoryTopic() == null || createUpdateCategoryDto.getCategoryTopic().isEmpty()){
            throw new CategoryIncorrect();
        }

        Optional<Category> byId = categoryRepository.findById(id);



        if(byId.isPresent()){
            Category category = byId.get();
            category.setCategoryTopic(createUpdateCategoryDto.getCategoryTopic());

            Category updatedCategory = categoryRepository.save(category);
            return categoryDtoMapper.toDto(updatedCategory);
        }else{
            throw new CategoryNotFound();
        }
    }

    // delete
    public CategoryDto deleteCategory(Integer id) throws CategoryNotFound {
        Optional<Category> byId = categoryRepository.findById(id);
        if(byId.isPresent()){
            Category category = byId.get();
            categoryRepository.delete(category);

            return categoryDtoMapper.toDto(category);
        }else{
            throw new CategoryNotFound();
        }
    }
}
