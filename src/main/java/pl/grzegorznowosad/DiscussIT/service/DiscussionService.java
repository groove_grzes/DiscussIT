package pl.grzegorznowosad.DiscussIT.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.grzegorznowosad.DiscussIT.controller.dto.*;
import pl.grzegorznowosad.DiscussIT.model.Discussion;
import pl.grzegorznowosad.DiscussIT.model.Section;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.DiscussionRepository;
import pl.grzegorznowosad.DiscussIT.repository.MessageRepository;
import pl.grzegorznowosad.DiscussIT.repository.SectionRepository;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.*;
import pl.grzegorznowosad.DiscussIT.service.mapper.DiscussionDtoMapper;
import pl.grzegorznowosad.DiscussIT.service.mapper.DiscussionViewDtoMapper;
import pl.grzegorznowosad.DiscussIT.service.mapper.MessageDtoMapper;
import pl.grzegorznowosad.DiscussIT.service.other.DateOfCreation;

import java.util.*;

@Service
public class DiscussionService {

    @Autowired
    private DiscussionRepository discussionRepository;

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private SectionService sectionService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DiscussionDtoMapper discussionDtoMapper;

    @Autowired
    private DiscussionViewDtoMapper discussionViewDtoMapper;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private MessageDtoMapper messageDtoMapper;

    // getAll
    public List<DiscussionDto> getAll(){
        List<Discussion> all = discussionRepository.findAll();
        List<DiscussionDto> allDtos = new ArrayList<>();

        all.forEach(x -> {
            allDtos.add(discussionDtoMapper.toDto(x));
        });

        return allDtos;
    }

    // getById
    public DiscussionDto getById(Integer id) throws DiscussionNotFound {
        Optional<Discussion> byId = discussionRepository.findById(id);

        if(byId.isPresent()){
            Discussion discussion = byId.get();

            return discussionDtoMapper.toDto(discussion);
        }else{
            throw new DiscussionNotFound();
        }
    }

    // getByTopic
    public DiscussionDto getByDiscussionTopic(String topic) throws DiscussionNotFound {
        Optional<Discussion> byTopic = discussionRepository.findByTopic(topic);

        if(byTopic.isPresent()){
            Discussion discussion = byTopic.get();

            return discussionDtoMapper.toDto(discussion);
        }else{
            throw new DiscussionNotFound();
        }
    }

    // create
    public DiscussionDto createDiscussion(CreateDiscussionDto createDiscussionDto) throws DiscussionIncorrect, DiscussionAlreadyExist, DiscussionNotFound {
        if(createDiscussionDto.getTopic() == null || createDiscussionDto.getTopic().isEmpty()){
            throw new DiscussionIncorrect();
        }

        if(discussionRepository.findByTopic(createDiscussionDto.getTopic()).isPresent()){
            throw new DiscussionAlreadyExist();
        }

        DateOfCreation date = new DateOfCreation();

        Discussion savedDiscussion = discussionRepository.save(discussionDtoMapper.toModel(createDiscussionDto, date.getDate()));

        return discussionDtoMapper.toDto(savedDiscussion);
    }

    // create discussion from View
    public DiscussionDto createDiscussionFromView(CreateDiscussionViewDto createDiscussionViewDto) throws DiscussionIncorrect, DiscussionAlreadyExist {
        if(createDiscussionViewDto.getTopic() == null || createDiscussionViewDto.getTopic().isEmpty()){
            throw new DiscussionIncorrect();
        }

        if(discussionRepository.findByTopic(createDiscussionViewDto.getTopic()).isPresent()){
            throw new DiscussionAlreadyExist();
        }

        DateOfCreation date = new DateOfCreation();

        Discussion savedDiscussion = discussionRepository.save(discussionViewDtoMapper.toModel(createDiscussionViewDto, date.getDate()));

        return discussionDtoMapper.toDto(savedDiscussion);
    }

    @Transactional
    public void saveNewDiscussion(CreateDiscussionViewDto createDiscussionViewDto) throws DiscussionIncorrect, DiscussionAlreadyExist, MessageIncorrect {
        if(createDiscussionViewDto.getTopic() == null || createDiscussionViewDto.getTopic().isEmpty()){
            throw new DiscussionIncorrect();
        }

        if(discussionRepository.findByTopic(createDiscussionViewDto.getTopic()).isPresent()){
            throw new DiscussionAlreadyExist();
        }

        DateOfCreation date = new DateOfCreation();

        Discussion savedDiscussion = discussionRepository.save(discussionViewDtoMapper.toModel(createDiscussionViewDto, date.getDate()));

        CreateMessageDto createMessageDto = new CreateMessageDto(createDiscussionViewDto.getTextMessage(), savedDiscussion.getUser().getId(), savedDiscussion.getId());

        messageRepository.save(messageDtoMapper.toModel(createMessageDto, savedDiscussion.getDate()));
    }


    // update
    public DiscussionDto updateDiscussion(UpdateDiscussionDto updateDiscussionDto, Integer id) throws DiscussionIncorrect {
        Optional<Discussion> byId = discussionRepository.findById(id);

        if(byId.isPresent()){
            Discussion discussion = byId.get();

            Optional<User> userById = userRepository.findById(discussion.getUser().getId());
            Optional<Section> sectionById = sectionRepository.findById(discussion.getSection().getId());

            if(userById.isPresent() && sectionById.isPresent()){
                discussion.setTopic(updateDiscussionDto.getTopic());
                discussion.setSection(sectionById.get());
                discussion.setUser(userById.get());
            }else{
                throw new DiscussionIncorrect();
            }

            Discussion discussionSaved = discussionRepository.save(discussion);

            return discussionDtoMapper.toDto(discussionSaved);
        }else{
            throw new DiscussionIncorrect();
        }
    }

    // delete
    public DiscussionDto deleteDiscussion(Integer id) throws DiscussionNotFound {
        Optional<Discussion> byId = discussionRepository.findById(id);

        if(byId.isPresent()){
            Discussion discussion = byId.get();

            discussionRepository.delete(discussion);

            return discussionDtoMapper.toDto(discussion);
        }else{
            throw new DiscussionNotFound();
        }
    }

    // --------------------
    // getDiscussionBySectionId
    public List<DiscussionViewDto> getDiscussionsBySectionId(Integer id) throws SectionNotFound {
        Optional<Section> sectionById = sectionRepository.findById(id);

        List<DiscussionDto> allDiscussionDto = new ArrayList<>();
        List<DiscussionViewDto> allDiscussionViewDto = new ArrayList<>();

        if(sectionById.isPresent()){
            Section section = sectionById.get();

            List<Discussion> allBySection = discussionRepository.findAllBySection(section);

            allBySection.forEach(x -> {
                try {
                    allDiscussionViewDto.add(discussionViewDtoMapper.toDto(x));
                } catch (UserNotFound userNotFound) {
                    userNotFound.printStackTrace();
                }
            });

            return allDiscussionViewDto;
        }else{
            throw new SectionNotFound();
        }
    }

    public Map<Integer, Integer> getNumberOfDiscussions(){
        List<DiscussionDto> allDiscussions = getAll();
        List<SectionDto> allSections = sectionService.getAll();
        List<SectionWithDiscussionNumberDto> listSectionWithDiscussionNumberDto = new ArrayList<>();

        Map<Integer, Integer> mapOfSomething = new HashMap<>();

            for(SectionDto section : allSections){
                int counter = 0;
                for(DiscussionDto discussion : allDiscussions) {

                    if(section.getId() == discussion.getSectionId()){
                        counter++;
                    }
                }
                listSectionWithDiscussionNumberDto.add(new SectionWithDiscussionNumberDto(section.getId(), counter));

                mapOfSomething.put(section.getId(), counter);
            }

        return mapOfSomething;
    }

    public List<DiscussionDto> findTop5Desc(){
        List<Discussion> top5ByOrderByIdDesc = discussionRepository.findTop5ByOrderByIdDesc();
        List<DiscussionDto> discussionDtoTop5 = new ArrayList<>();

        top5ByOrderByIdDesc.forEach(x -> {
            discussionDtoTop5.add(discussionDtoMapper.toDto(x));
        });

        return discussionDtoTop5;
    }

    public Integer numberOfDiscussionsByUser(Integer id) throws UserNotFound {
        Optional<User> userById = userRepository.findById(id);

        if(userById.isPresent()){

            User user = userById.get();
            List<Discussion> allDiscussionsByUser = discussionRepository.findAllByUser(user);

            return allDiscussionsByUser.size();

        }else{
            throw new UserNotFound();
        }
    }
}
