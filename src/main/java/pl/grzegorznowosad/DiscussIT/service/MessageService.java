package pl.grzegorznowosad.DiscussIT.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.DiscussIT.controller.dto.*;
import pl.grzegorznowosad.DiscussIT.model.Discussion;
import pl.grzegorznowosad.DiscussIT.model.Message;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.DiscussionRepository;
import pl.grzegorznowosad.DiscussIT.repository.MessageRepository;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.*;
import pl.grzegorznowosad.DiscussIT.service.mapper.MessageDtoMapper;
import pl.grzegorznowosad.DiscussIT.service.mapper.MessageViewDtoMapper;
import pl.grzegorznowosad.DiscussIT.service.mapper.MessageViewFrontPageDtoMapper;
import pl.grzegorznowosad.DiscussIT.service.mapper.UserDtoMapper;
import pl.grzegorznowosad.DiscussIT.service.other.DateOfCreation;

import java.sql.SQLException;
import java.util.*;

@Service
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private MessageDtoMapper messageDtoMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DiscussionRepository discussionRepository;

    @Autowired
    private MessageViewDtoMapper messageViewDtoMapper;

    @Autowired
    private MessageViewFrontPageDtoMapper messageViewFrontPageDtoMapper;

    @Autowired
    private UserService userService;


    // getAll
    public List<MessageDto> getAll(){
        List<Message> all = messageRepository.findAll();
        List<MessageDto> allDtos = new ArrayList<>();

        all.forEach(x -> {
            allDtos.add(messageDtoMapper.toDto(x));
        });

        return allDtos;
    }

    // getById
    public MessageDto getById(Integer id) throws MessageNotFound {
        Optional<Message> byId = messageRepository.findById(id);

        if(byId.isPresent()){
            Message message = byId.get();

            return messageDtoMapper.toDto(message);
        }else{
            throw new MessageNotFound();
        }
    }

    // getByText
    public MessageDto getByText(String text) throws MessageNotFound {
        Optional<Message> byText = messageRepository.findByText(text);

        if(byText.isPresent()){
            Message message = byText.get();

            return messageDtoMapper.toDto(message);
        }else{
            throw new MessageNotFound();
        }
    }

    // all by user
    public List<MessageViewDto> getAllByUser(Integer id) throws UserNotFound, SQLException {
        Optional<User> userById = userRepository.findById(id);

        if(userById.isPresent()){
            User user = userById.get();

            List<Message> allMessagesByUser = messageRepository.findAllByUser(user);
            List<MessageViewDto> allMessagesByUserView = new ArrayList<>();

            for(Message message : allMessagesByUser){
                allMessagesByUserView.add(messageViewDtoMapper.toDto(message));
            }

            return allMessagesByUserView;
        }else{
            throw new UserNotFound();
        }
    }

    // create
    public MessageDto createMessage(CreateMessageDto messageDto) throws MessageIncorrect, MessageAlreadyExist {
        if(messageDto.getText() == null || messageDto.getText().isEmpty()){
            throw new MessageIncorrect();
        }

        if(messageRepository.findByText(messageDto.getText()).isPresent()){
            throw new MessageAlreadyExist();
        }

        DateOfCreation date = new DateOfCreation();

        Message savedMessage = messageRepository.save(messageDtoMapper.toModel(messageDto, date.getDate()));

        return messageDtoMapper.toDto(savedMessage);
    }

    // create
    public MessageDto createMessageFromView(CreateMessageViewDto createMessageViewDto) throws MessageIncorrect, UserNotFound {
        if(createMessageViewDto.getText() == null || createMessageViewDto.getText().isEmpty()){
            throw new MessageIncorrect();
        }

        DateOfCreation date = new DateOfCreation();
        Message savedMessage = messageRepository.save(messageViewDtoMapper.toModel(createMessageViewDto, date.getDate()));

        return messageDtoMapper.toDto(savedMessage);
    }


    // update
    public MessageDto updateMessage(UpdateMessageDto updateMessageDto, Integer id) throws MessageNotFound, MessageIncorrect {
        Optional<Message> byId = messageRepository.findById(id);

        if(byId.isPresent()){
            Message message = byId.get();

            Optional<User> userById = userRepository.findById(updateMessageDto.getUserId());
            Optional<Discussion> discussionById = discussionRepository.findById(updateMessageDto.getDiscussionId());

            if(userById.isPresent() && discussionById.isPresent()){
                User user = userById.get();
                Discussion discussion = discussionById.get();

                message.setText(updateMessageDto.getText());
                message.setUser(user);
                message.setDiscussion(discussion);

            }else{
                throw new MessageIncorrect();
            }

            Message savedMessage = messageRepository.save(message);

            return messageDtoMapper.toDto(savedMessage);
        }else{
            throw new MessageNotFound();
        }
    }

    // delete
    public MessageDto deleteMessage(Integer id) throws MessageNotFound {
        Optional<Message> byId = messageRepository.findById(id);

        if(byId.isPresent()){
            Message message = byId.get();

            messageRepository.delete(message);

            return messageDtoMapper.toDto(message);
        }else{
            throw new MessageNotFound();
        }
    }

    // -------------------------

    // getMessagesByDiscussionId
    public List<MessageViewDto> getMessagesByDiscussionId(Integer id) throws DiscussionNotFound, UserNotFound, SQLException {

        Optional<Discussion> discussionById = discussionRepository.findById(id);
        List<MessageViewDto> allMessageViewDtos = new ArrayList<>();

        if(discussionById.isPresent()){
            Discussion discussion = discussionById.get();

            List<Message> allMessagesByDiscussion = messageRepository.findAllByDiscussion(discussion);

            for(Message message : allMessagesByDiscussion){
                allMessageViewDtos.add(messageViewDtoMapper.toDto(message));
            }

            return allMessageViewDtos;
        }else {
            throw new DiscussionNotFound();
        }
    }

    // last 10 messages
    public List<MessageViewFrontPageDto> getLast10Messages() throws UserNotFound {
        List<Message> last10Messages = messageRepository.findTop10ByOrderByIdDesc();
        List<MessageViewFrontPageDto> last10MessagesViewDto = new ArrayList<>();

        int maxStringLength = 20;

        for(Message message : last10Messages){

            String text = message.getText();
            StringBuilder textShorted = new StringBuilder();

            if(text.length() < maxStringLength){
                textShorted.append(text);
            }else{
                for(int i = 0; i < maxStringLength; i++){
                    textShorted.append(text.charAt(i));
                }
                textShorted.append(" ...");
            }

            message.setText(textShorted.toString());

            last10MessagesViewDto.add(messageViewFrontPageDtoMapper.toDto(message));
        }

        return last10MessagesViewDto;
    }

    //
    public Integer numberOfMessagesByAuthor(Integer id) throws UserNotFound {

        Optional<User> userById = userRepository.findById(id);

        if(userById.isPresent()){
            User user = userById.get();

            List<Message> allMessagesByUser = messageRepository.findAllByUser(user);

            return allMessagesByUser.size();
        }else{
            throw new UserNotFound();
        }

    }

    public Map<Integer, Integer> numberOfMessagesByUser() throws UserNotFound, DiscussionNotFound, SQLException {

        Map<Integer, Integer> mapOfNumberMessagesForUser = new HashMap<>();

        List<UserDto> allUsers = userService.getAll();

        for(UserDto userDto : allUsers){
            List<MessageViewDto> allByUser = getAllByUser(userDto.getId());
            mapOfNumberMessagesForUser.put(userDto.getId(), allByUser.size());
        }


        return mapOfNumberMessagesForUser;
    }
}
