package pl.grzegorznowosad.DiscussIT.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.DiscussIT.controller.dto.*;
import pl.grzegorznowosad.DiscussIT.model.PrivateMessage;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.PrivateMessageRepository;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.PrivateMessageDataIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.PrivateMessageNotFound;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;
import pl.grzegorznowosad.DiscussIT.service.mapper.PrivateMessageDtoMapper;
import pl.grzegorznowosad.DiscussIT.service.mapper.PrivateMessageViewDtoMapper;
import pl.grzegorznowosad.DiscussIT.service.mapper.UserDtoMapper;
import pl.grzegorznowosad.DiscussIT.service.other.DateOfCreation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PrivateMessageService {

    @Autowired
    private PrivateMessageRepository privateMessageRepository;

    @Autowired
    private PrivateMessageDtoMapper privateMessageDtoMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private PrivateMessageViewDtoMapper privateMessageViewDtoMapper;


    //getAll
    public List<PrivateMessageDto> getAll(){
        List<PrivateMessage> allPrivateMessages = privateMessageRepository.findAll();
        List<PrivateMessageDto> allPrivateMessagesDto = new ArrayList<>();

        allPrivateMessages.forEach(x -> {
            allPrivateMessagesDto.add(privateMessageDtoMapper.toDto(x));
        });

        return allPrivateMessagesDto;
    }

    // getAllByReceiver
    public List<PrivateMessageViewDto> getAllByReceiver(Integer id) throws UserNotFound {

        Optional<User> userById = userRepository.findById(id);

        if(userById.isPresent()){
            User user = userById.get();
            List<PrivateMessage> allPrivateMessagesByReceiver = privateMessageRepository.findAllByReceiverOrderByIdDesc(user);

            List<PrivateMessageViewDto> allPrivateMessagesViewDtoByReceiver = new ArrayList<>();

            for(PrivateMessage pm : allPrivateMessagesByReceiver){
                allPrivateMessagesViewDtoByReceiver.add(privateMessageViewDtoMapper.toDto(pm));
            }

            return allPrivateMessagesViewDtoByReceiver;

        }else{
            throw new UserNotFound();
        }
    }

    // getAllBySender
    public List<PrivateMessageViewDto> getAllBySender(Integer id) throws UserNotFound {

        Optional<User> userById = userRepository.findById(id);

        if(userById.isPresent()){
            User user = userById.get();
            List<PrivateMessage> allPrivateMessagesBySender = privateMessageRepository.findAllBySenderOrderByIdDesc(user);

            List<PrivateMessageViewDto> allPrivateMessageViewDtoBySender = new ArrayList<>();

            for(PrivateMessage pm : allPrivateMessagesBySender){
                allPrivateMessageViewDtoBySender.add(privateMessageViewDtoMapper.toDto(pm));
            }

            return allPrivateMessageViewDtoBySender;

        }else{
            throw new UserNotFound();
        }
    }


    //getById
    public PrivateMessageDto getById(Integer id) throws PrivateMessageNotFound {
        Optional<PrivateMessage> privateMessageById = privateMessageRepository.findById(id);

        if(privateMessageById.isPresent()){

            PrivateMessage privateMessage = privateMessageById.get();

            return privateMessageDtoMapper.toDto(privateMessage);

        }else{
            throw new PrivateMessageNotFound();
        }
    }

    // create
    public PrivateMessageDto create(CreatePrivateMessageDto createPrivateMessageDto) throws UserNotFound, PrivateMessageDataIncorrect {

        if(createPrivateMessageDto.getText() == null || createPrivateMessageDto.getText().isEmpty()){
            throw new PrivateMessageDataIncorrect();
        }

        DateOfCreation dateOfCreation = new DateOfCreation();

        PrivateMessage savedPrivateMessage = privateMessageRepository.save(privateMessageDtoMapper.toModel(createPrivateMessageDto, dateOfCreation.getDate()));

        return privateMessageDtoMapper.toDto(savedPrivateMessage);
    }

    // update
    public PrivateMessageDto update(UpdatePrivateMessageDto updatePrivateMessageDto, Integer id) throws PrivateMessageNotFound, UserNotFound {
        Optional<PrivateMessage> privateMessageById = privateMessageRepository.findById(id);


        if(privateMessageById.isPresent()){
            PrivateMessage privateMessage = privateMessageById.get();

            Optional<User> senderById = userRepository.findById(updatePrivateMessageDto.getSenderId());
            Optional<User> receiverById = userRepository.findById(updatePrivateMessageDto.getReceiverId());

            if(senderById.isPresent() && receiverById.isPresent()){
                User sender = senderById.get();
                User receiver = receiverById.get();

                privateMessage.setText(updatePrivateMessageDto.getText());
                privateMessage.setIsNew(updatePrivateMessageDto.getIsNew());
                privateMessage.setReceiver(receiver);
                privateMessage.setSender(sender);

                PrivateMessage savedPrivateMessage = privateMessageRepository.save(privateMessage);

                return privateMessageDtoMapper.toDto(savedPrivateMessage);

            }else{
                throw new UserNotFound();
            }
        }else{
            throw new PrivateMessageNotFound();
        }
    }

    // update, mark as read
    public PrivateMessageDto markAsRead(Integer id) throws PrivateMessageNotFound {
        Optional<PrivateMessage> pmById = privateMessageRepository.findById(id);

        if(pmById.isPresent()){
            PrivateMessage privateMessage = pmById.get();

            privateMessage.setIsNew("no");
            privateMessageRepository.save(privateMessage);

            return privateMessageDtoMapper.toDto(privateMessage);

        }else{
            throw new PrivateMessageNotFound();
        }
    }

    // update, mark as read
    public PrivateMessageDto markAsUnread(Integer id) throws PrivateMessageNotFound {
        Optional<PrivateMessage> pmById = privateMessageRepository.findById(id);

        if(pmById.isPresent()){
            PrivateMessage privateMessage = pmById.get();

            privateMessage.setIsNew("yes");
            privateMessageRepository.save(privateMessage);

            return privateMessageDtoMapper.toDto(privateMessage);

        }else{
            throw new PrivateMessageNotFound();
        }
    }



    // delete
    public PrivateMessageDto delete(Integer id) throws PrivateMessageNotFound {
        Optional<PrivateMessage> privateMessageById = privateMessageRepository.findById(id);

        if(privateMessageById.isPresent()){
            PrivateMessage privateMessage = privateMessageById.get();

            privateMessageRepository.delete(privateMessage);

            return privateMessageDtoMapper.toDto(privateMessage);
        }else{
            throw new PrivateMessageNotFound();
        }
    }

    // after reading message (change isNew to "no"
    public PrivateMessageDto readMessage(Integer id) throws PrivateMessageDataIncorrect {
        Optional<PrivateMessage> privateMessageById = privateMessageRepository.findById(id);

        if(privateMessageById.isPresent()){
            PrivateMessage privateMessage = privateMessageById.get();
            privateMessage.setIsNew("no");

            PrivateMessage savedPrivateMessage = privateMessageRepository.save(privateMessage);

            return privateMessageDtoMapper.toDto(savedPrivateMessage);
        }else{
            throw new PrivateMessageDataIncorrect();
        }
    }

    // getAllMessasgesNewForUser
    public List<PrivateMessageDto> getAllNewPrivateMessagesForUser(Integer id) throws UserNotFound {
        Optional<User> userById = userRepository.findById(id);

        if(userById.isPresent()){
            User user = userById.get();

            List<PrivateMessage> listOfNewMessagesForUser = privateMessageRepository.findAllByReceiverAndIsNewOrderByIdDesc(user, "yes");
            List<PrivateMessageDto> allNewMessagesForUser = new ArrayList<>();

            listOfNewMessagesForUser.forEach(x -> {
                allNewMessagesForUser.add(privateMessageDtoMapper.toDto(x));
            });

            return allNewMessagesForUser;
        }else{
            throw new UserNotFound();
        }
    }

    public List<PrivateMessageDto> getAllOldPrivateMessagesForUser(Integer id) throws UserNotFound {
        Optional<User> userById = userRepository.findById(id);

        if(userById.isPresent()){
            User user = userById.get();

            List<PrivateMessage> listOfNewMessagesForUser = privateMessageRepository.findAllByReceiverAndIsNewOrderByIdDesc(user, "no");
            List<PrivateMessageDto> allNewMessagesForUser = new ArrayList<>();

            listOfNewMessagesForUser.forEach(x -> {
                allNewMessagesForUser.add(privateMessageDtoMapper.toDto(x));
            });

            return allNewMessagesForUser;
        }else{
            throw new UserNotFound();
        }
    }


}
