package pl.grzegorznowosad.DiscussIT.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateUpdateRoleApplication;
import pl.grzegorznowosad.DiscussIT.controller.dto.RoleApplicationDto;
import pl.grzegorznowosad.DiscussIT.model.RoleApplication;
import pl.grzegorznowosad.DiscussIT.repository.RoleApplicationRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.RoleApplicationAlreadyExist;
import pl.grzegorznowosad.DiscussIT.service.exception.RoleApplicationDataIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.RoleApplicationNotFound;
import pl.grzegorznowosad.DiscussIT.service.mapper.RoleApplicationDtoMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RoleApplicationService {

    @Autowired
    private RoleApplicationRepository roleApplicationRepository;

    @Autowired
    private RoleApplicationDtoMapper roleApplicationDtoMapper;

    // getAll
    public List<RoleApplicationDto> getAll(){
        List<RoleApplication> allRoles = roleApplicationRepository.findAll();
        List<RoleApplicationDto> allRolesDto = new ArrayList<>();

        allRoles.forEach(x -> {
            allRolesDto.add(roleApplicationDtoMapper.toDto(x));
        });

        return allRolesDto;
    }


    // getById
    public RoleApplicationDto getById(Integer id) throws RoleApplicationNotFound {
        Optional<RoleApplication> roleById = roleApplicationRepository.findById(id);

        if(roleById.isPresent()){
            RoleApplication roleApplication = roleById.get();

            return roleApplicationDtoMapper.toDto(roleApplication);

        }else{
            throw new RoleApplicationNotFound();
        }
    }

    // getByRoleName
    public RoleApplicationDto getByRoleName(String roleName) throws RoleApplicationNotFound {
        Optional<RoleApplication> roleByName = roleApplicationRepository.findByRoleName(roleName);

        if(roleByName.isPresent()){
            RoleApplication roleApplication = roleByName.get();

            return roleApplicationDtoMapper.toDto(roleApplication);
        }else{
            throw new RoleApplicationNotFound();
        }
    }

    // create
    public RoleApplicationDto create(CreateUpdateRoleApplication createUpdateRoleApplication) throws RoleApplicationDataIncorrect, RoleApplicationAlreadyExist {
        if(createUpdateRoleApplication.getRoleName().isEmpty() || createUpdateRoleApplication.getRoleName() == null){
            throw new RoleApplicationDataIncorrect();
        }

        if(roleApplicationRepository.findByRoleName(createUpdateRoleApplication.getRoleName()).isPresent()){
            throw new RoleApplicationAlreadyExist();
        }

        RoleApplication roleSaved = roleApplicationRepository.save(roleApplicationDtoMapper.toModel(createUpdateRoleApplication));

        return roleApplicationDtoMapper.toDto(roleSaved);
    }

    // update
    public RoleApplicationDto update(Integer id, CreateUpdateRoleApplication createUpdateRoleApplication) throws RoleApplicationDataIncorrect {
        Optional<RoleApplication> roleById = roleApplicationRepository.findById(id);

        if(roleById.isPresent() && createUpdateRoleApplication.getRoleName() != null && !createUpdateRoleApplication.getRoleName().isEmpty()){

            RoleApplication role = roleById.get();

            role.setRoleName(createUpdateRoleApplication.getRoleName());

            RoleApplication roleSaved = roleApplicationRepository.save(role);

            return roleApplicationDtoMapper.toDto(roleSaved);

        }else{
            throw new RoleApplicationDataIncorrect();
        }
    }

    // delete
    public RoleApplicationDto delete(Integer id) throws RoleApplicationAlreadyExist {
        Optional<RoleApplication> roleById = roleApplicationRepository.findById(id);

        if(roleById.isPresent()){
            RoleApplication roleApplication = roleById.get();

            roleApplicationRepository.delete(roleApplication);

            return roleApplicationDtoMapper.toDto(roleApplication);
        }else{
            throw new RoleApplicationAlreadyExist();
        }
    }
}
