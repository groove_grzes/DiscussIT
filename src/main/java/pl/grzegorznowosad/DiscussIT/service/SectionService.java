package pl.grzegorznowosad.DiscussIT.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.DiscussIT.controller.dto.*;
import pl.grzegorznowosad.DiscussIT.model.Category;
import pl.grzegorznowosad.DiscussIT.model.Section;
import pl.grzegorznowosad.DiscussIT.repository.CategoryRepository;
import pl.grzegorznowosad.DiscussIT.repository.SectionRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.*;
import pl.grzegorznowosad.DiscussIT.service.mapper.SectionDtoMapper;

import java.sql.SQLException;
import java.util.*;

@Service
public class SectionService {

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private SectionDtoMapper sectionDtoMapper;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private DiscussionService discussionService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private CategoryService categoryService;

    // getAll
    public List<SectionDto> getAll(){
        List<Section> all = sectionRepository.findAll();
        List<SectionDto> allDtos = new ArrayList<>();

        all.forEach(x -> {
            allDtos.add(sectionDtoMapper.toDto(x));
        });

        return allDtos;
    }

    // getById
    public SectionDto getById(Integer id) throws SectionNotFound {
        Optional<Section> byId = sectionRepository.findById(id);

        if(byId.isPresent()){
            Section section = byId.get();
            return sectionDtoMapper.toDto(section);
        }else{
            throw new SectionNotFound();
        }
    }

    // getBySectionTopic
    public SectionDto getBySectionTopic(String topic) throws SectionNotFound {
        Optional<Section> bySectionTopic = sectionRepository.findBySectionTopic(topic);

        if(bySectionTopic.isPresent()){
            Section section = bySectionTopic.get();

            return sectionDtoMapper.toDto(section);
        }else{
            throw new SectionNotFound();
        }
    }

    // createSection
    public SectionDto createSection(CreateUpdateSectionDto createUpdateSectionDto) throws SectionIncorrect, SectionAlreadyExist, CategoryNotFound {
        if(createUpdateSectionDto.getSectionTopic() == null || createUpdateSectionDto.getSectionTopic().isEmpty()){
            throw new SectionIncorrect();
        }

        if(sectionRepository.findBySectionTopic(createUpdateSectionDto.getSectionTopic()).isPresent()){
            throw new SectionAlreadyExist();
        }

        Section saveSaved = sectionRepository.save(sectionDtoMapper.toModel(createUpdateSectionDto));

        return sectionDtoMapper.toDto(saveSaved);
    }

    // updateSection
    public SectionDto updateSection(Integer id, CreateUpdateSectionDto createUpdateSectionDto) throws SectionIncorrect, SectionNotFound {
        if(createUpdateSectionDto.getSectionTopic() == null || createUpdateSectionDto.getSectionTopic().isEmpty()){
            throw new SectionIncorrect();
        }

        Optional<Section> byId = sectionRepository.findById(id);
        if(byId.isPresent()){
            Section section = byId.get();
            section.setSectionTopic(createUpdateSectionDto.getSectionTopic());

            Section save = sectionRepository.save(section);
            return sectionDtoMapper.toDto(section);
        }else{
            throw new SectionNotFound();
        }
    }

    // deleteSection
    public SectionDto deleteSection(Integer id) throws SectionNotFound {
        Optional<Section> byId = sectionRepository.findById(id);

        if(byId.isPresent()){
            Section section = byId.get();

            sectionRepository.delete(section);

            return sectionDtoMapper.toDto(section);
        }else{
            throw new SectionNotFound();
        }
    }

    public List<SectionDto> getSectionByCategoryId(Integer id) throws CategoryNotFound {
        Optional<Category> categoryById = categoryRepository.findById(id);

        if(categoryById.isPresent()) {
            Category category = categoryById.get();

            List<Section> sectionsByCategory = sectionRepository.findByCategory(category);
            List<SectionDto> sectionsDtoByCategory = new ArrayList<>();

            sectionsByCategory.forEach(x -> {
                sectionsDtoByCategory.add(sectionDtoMapper.toDto(x));
            });

            return sectionsDtoByCategory;

        }else{
            throw new CategoryNotFound();
        }
    }

    public Map<Integer, SectionNumbersFrontPage> getDataForSectionsForFrontPage() throws CategoryNotFound, SectionNotFound, DiscussionNotFound, UserNotFound, SQLException {
        // liczby dla poszczegolnych sekcji
        Map<Integer, SectionNumbersFrontPage> sectionNumbers = new HashMap<>();

        List<CategoryDto> allCategories = categoryService.getAll();

        for(CategoryDto category : allCategories){

            // pobieram wszystkie sekcje per category
            List<SectionDto> listSectionsByCategoryId = getSectionByCategoryId(category.getId());

            for(SectionDto section : listSectionsByCategoryId){
                Integer sectionId = section.getId();

                List<DiscussionViewDto> listDiscussionsBySectionId = discussionService.getDiscussionsBySectionId(section.getId());
                Integer numberOfAllDiscussions = listDiscussionsBySectionId.size();

                String authorLogin = "null";
                Integer authorLoginId = new Integer(0);
                Integer numberOfAllMessages = new Integer(0);
                String date = null;
                String userAvatar = null;

                if(numberOfAllDiscussions != 0) {

                    for(DiscussionViewDto discussion : listDiscussionsBySectionId){
                        List<MessageViewDto> messagesByDiscussionId = messageService.getMessagesByDiscussionId(discussion.getId());

                        numberOfAllMessages += messagesByDiscussionId.size();
                    }

                    DiscussionViewDto lastDiscussion = listDiscussionsBySectionId.get(listDiscussionsBySectionId.size() - 1);

                    List<MessageViewDto> messagesByLastDiscussion = messageService.getMessagesByDiscussionId(lastDiscussion.getId());

                    if(messagesByLastDiscussion.size() != 0){
                        MessageViewDto lastMessage = messagesByLastDiscussion.get(messagesByLastDiscussion.size() - 1);

                        authorLogin = lastMessage.getUserLogin();
                        authorLoginId = lastMessage.getUserId();
                        date = lastMessage.getDate();
                        userAvatar = lastMessage.getAvatar();
                    }
                }

                SectionNumbersFrontPage sectionNumbersFrontPage = new SectionNumbersFrontPage(numberOfAllDiscussions, numberOfAllMessages, authorLogin, authorLoginId, date, userAvatar);
                sectionNumbers.put(sectionId, sectionNumbersFrontPage);

            }
        }

        return sectionNumbers;
    }

}
