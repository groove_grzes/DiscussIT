package pl.grzegorznowosad.DiscussIT.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateUpdateShoutboxMessageDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.ShoutboxMessageDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.ShoutboxMessageViewDto;
import pl.grzegorznowosad.DiscussIT.model.ShoutboxMessage;
import pl.grzegorznowosad.DiscussIT.repository.ShoutboxMessageRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.ShoutboxMessageIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.ShoutboxMessageNotFound;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;
import pl.grzegorznowosad.DiscussIT.service.mapper.ShoutboxMessageDtoMapper;
import pl.grzegorznowosad.DiscussIT.service.mapper.ShoutboxMessageViewDtoMapper;
import pl.grzegorznowosad.DiscussIT.service.other.DateOfCreation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ShoutboxService {

    @Autowired
    private ShoutboxMessageRepository shoutboxMessageRepository;

    @Autowired
    private ShoutboxMessageDtoMapper shoutboxMessageDtoMapper;

    @Autowired
    private ShoutboxMessageViewDtoMapper shoutboxMessageViewDtoMapper;

    // getAll
    public List<ShoutboxMessageDto> getAll(){
        List<ShoutboxMessage> allShoutboxMessages = shoutboxMessageRepository.findAll();
        List<ShoutboxMessageDto> allShoutboxMessagesDto = new ArrayList<>();

        allShoutboxMessages.forEach(x -> {
            allShoutboxMessagesDto.add(shoutboxMessageDtoMapper.toDto(x));
        });

        return allShoutboxMessagesDto;
    }

    // getById
    public ShoutboxMessageDto getById(Integer id) throws ShoutboxMessageNotFound {
        Optional<ShoutboxMessage> shoutBoxMessageById = shoutboxMessageRepository.findById(id);

        if(shoutBoxMessageById.isPresent()){
            ShoutboxMessage shoutboxMessage = shoutBoxMessageById.get();

            return shoutboxMessageDtoMapper.toDto(shoutboxMessage);

        }else{
            throw new ShoutboxMessageNotFound();
        }
    }

    // getByText
    public ShoutboxMessageDto getByText(String text) throws ShoutboxMessageNotFound {
        Optional<ShoutboxMessage> shoutboxMessageByText = shoutboxMessageRepository.findByText(text);

        if(shoutboxMessageByText.isPresent()){
            ShoutboxMessage shoutboxMessage = shoutboxMessageByText.get();

            return shoutboxMessageDtoMapper.toDto(shoutboxMessage);
        }else{
            throw new ShoutboxMessageNotFound();
        }
    }

    // create
    public ShoutboxMessageDto create(CreateUpdateShoutboxMessageDto createUpdateShoutboxMessageDto) throws ShoutboxMessageIncorrect, UserNotFound {
        if(createUpdateShoutboxMessageDto.getText() == null || createUpdateShoutboxMessageDto.getText().isEmpty()){
            throw new ShoutboxMessageIncorrect();
        }

        DateOfCreation date = new DateOfCreation();
        ShoutboxMessage shoutboxMessageSaved = shoutboxMessageRepository.save(shoutboxMessageDtoMapper.toModel(createUpdateShoutboxMessageDto,  date.getDate()));

        return shoutboxMessageDtoMapper.toDto(shoutboxMessageSaved);
    }

    // update
    public ShoutboxMessageDto update(CreateUpdateShoutboxMessageDto createUpdateShoutboxMessageDto, Integer id) throws ShoutboxMessageNotFound {
        Optional<ShoutboxMessage> shoutboxMessageById = shoutboxMessageRepository.findById(id);

        if(shoutboxMessageById.isPresent()){

            ShoutboxMessage shoutboxMessage = shoutboxMessageById.get();

            shoutboxMessage.setText(createUpdateShoutboxMessageDto.getText());

            ShoutboxMessage shoutboxMessageSaved = shoutboxMessageRepository.save(shoutboxMessage);

            return shoutboxMessageDtoMapper.toDto(shoutboxMessageSaved);

        }else{
            throw new ShoutboxMessageNotFound();
        }
    }

    // delete
    public ShoutboxMessageDto delete(Integer id) throws ShoutboxMessageNotFound {
        Optional<ShoutboxMessage> shoutboxMessageById = shoutboxMessageRepository.findById(id);

        if(shoutboxMessageById.isPresent()){
            ShoutboxMessage shoutboxMessage = shoutboxMessageById.get();

            shoutboxMessageRepository.delete(shoutboxMessage);

            return shoutboxMessageDtoMapper.toDto(shoutboxMessage);
        }else{
            throw new ShoutboxMessageNotFound();
        }
    }

    //get top10desc
    public List<ShoutboxMessageViewDto> getTop10Desc() throws UserNotFound {
        List<ShoutboxMessage> top10byOrderByIdDesc = shoutboxMessageRepository.findTop10ByOrderByIdDesc();
        List<ShoutboxMessageViewDto> top10ShoutsMessagesDesc = new ArrayList<>();

        for(ShoutboxMessage message: top10byOrderByIdDesc){
            top10ShoutsMessagesDesc.add(shoutboxMessageViewDtoMapper.toDto(message));
        }

        return top10ShoutsMessagesDesc;
    }
}
