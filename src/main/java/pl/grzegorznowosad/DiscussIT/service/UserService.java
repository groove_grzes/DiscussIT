package pl.grzegorznowosad.DiscussIT.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.DiscussIT.controller.dto.RoleApplicationDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.UpdateUserDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.UserCreateDto;
import pl.grzegorznowosad.DiscussIT.model.RoleApplication;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.RoleApplicationRepository;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.LoginIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.RoleApplicationNotFound;
import pl.grzegorznowosad.DiscussIT.service.exception.UserAlreadyExist;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;
import pl.grzegorznowosad.DiscussIT.service.mapper.UserDtoMapper;
import pl.grzegorznowosad.DiscussIT.controller.dto.UserDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDtoMapper userDtoMapper;

    @Autowired
    private RoleApplicationRepository roleApplicationRepository;

    //getAll
    public List<UserDto> getAll(){
        List<User> all = userRepository.findAll();
        List<UserDto> allDtos = new ArrayList<>();

        all.forEach(x -> {
            allDtos.add(userDtoMapper.toDto(x));
        });

        return allDtos;
    }

    // getByID
    public UserDto getById(Integer id) throws UserNotFound {
        Optional<User> byId = userRepository.findById(id);
        if(byId.isPresent()){
            User user = byId.get();
            return userDtoMapper.toDto(user);
        }else{
            throw new UserNotFound();
        }
    }

    // getByLogin
    public UserDto getByLogin(String login) throws UserNotFound {
        Optional<User> byLogin = userRepository.findByLogin(login);
        if(byLogin.isPresent()){
            User user = byLogin.get();
            return userDtoMapper.toDto(user);
        }else{
            throw new UserNotFound();
        }
    }

    // create
    public UserDto createUser(UserCreateDto createDto) throws LoginIncorrect, UserAlreadyExist, RoleApplicationNotFound {
        if(createDto.getLogin() == null || createDto.getLogin().isEmpty()){
            throw new LoginIncorrect();
        }

        if(userRepository.findByLogin(createDto.getLogin()).isPresent()){
            throw new UserAlreadyExist();
        }

        String password = createDto.getPassword();

        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        createDto.setPassword(bcrypt.encode(password));

        User userSaved = userRepository.save(userDtoMapper.toModel(createDto));
        return userDtoMapper.toDto(userSaved);
    }

    // update
    public UserDto updateUser(Integer id, UpdateUserDto updateUserDto) throws UserNotFound {

        Optional<User> byId = userRepository.findById(id);
        if(byId.isPresent()){
            User user = byId.get();

            user.setFirstName(updateUserDto.getFirstName());
            user.setBirthYear(updateUserDto.getBirthYear());
            user.setEmail(updateUserDto.getEmail());
            user.setAvatar(updateUserDto.getAvatar());

            User userUpdated = userRepository.save(user);
            return userDtoMapper.toDto(userUpdated);

        }else{
            throw new UserNotFound();
        }

    }

    // delete
    public UserDto deleteUser(Integer id) throws UserNotFound {
        Optional<User> byId = userRepository.findById(id);
        if(byId.isPresent()){
            User user = byId.get();
            userRepository.delete(user);

            return userDtoMapper.toDto(user);
        }else{
            throw new UserNotFound();
        }
    }

    public User getFullUser(String userLogin) throws UserNotFound {
        Optional<User> userById = userRepository.findByLogin(userLogin);

        if(userById.isPresent()){
            User user = userById.get();

            return user;
        }else{
            throw new UserNotFound();
        }
    }

    public UserDto setNewRole(String userLogin, String newRole) throws RoleApplicationNotFound, UserNotFound {
        Optional<User> userByLogin = userRepository.findByLogin(userLogin);
        Optional<RoleApplication> roleName = roleApplicationRepository.findByRoleName(newRole);

        if(userByLogin.isPresent() && roleName.isPresent()){

            User user = userByLogin.get();
            RoleApplication roleApplication = roleName.get();


            user.setRole(roleApplication);

            User savedUser = userRepository.save(user);

            return userDtoMapper.toDto(savedUser);

        }else{
            throw new UserNotFound();
        }
    }

    public UserDto getLatestUser(){
        List<UserDto> all = getAll();

        return all.get(all.size() - 1);
    }
}
