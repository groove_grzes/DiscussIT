package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateUpdateCategoryDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.CategoryDto;
import pl.grzegorznowosad.DiscussIT.model.Category;

@Component
public class CategoryDtoMapper {

    public Category toModel(CreateUpdateCategoryDto createUpdateCategoryDto){
        return new Category(null, createUpdateCategoryDto.getCategoryTopic(), null);
    }

    public CategoryDto toDto(Category category){
        return new CategoryDto(category.getId(), category.getCategoryTopic());
    }

}
