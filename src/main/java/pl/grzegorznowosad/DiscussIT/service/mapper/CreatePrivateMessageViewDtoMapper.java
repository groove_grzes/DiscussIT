package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreatePrivateMessageViewDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.PrivateMessageViewDto;
import pl.grzegorznowosad.DiscussIT.model.PrivateMessage;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;
import pl.grzegorznowosad.DiscussIT.service.other.DateOfCreation;

import java.util.Optional;

@Component
public class CreatePrivateMessageViewDtoMapper {

    @Autowired
    private UserRepository userRepository;

    public PrivateMessage toModel(CreatePrivateMessageViewDto createPrivateMessageViewDto) throws UserNotFound {

        Optional<User> senderById = userRepository.findById(createPrivateMessageViewDto.getSenderId());
        Optional<User> receiverById = userRepository.findById(createPrivateMessageViewDto.getReceiverId());

        DateOfCreation date = new DateOfCreation();

        if(senderById.isPresent() && receiverById.isPresent()){
            User sender = senderById.get();
            User receiver = receiverById.get();

            return new PrivateMessage(null, date.getDate(), createPrivateMessageViewDto.getText(), "yes", sender, receiver);
        }else{
            throw new UserNotFound();
        }
    }
}
