package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateDiscussionDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.DiscussionDto;
import pl.grzegorznowosad.DiscussIT.model.Discussion;
import pl.grzegorznowosad.DiscussIT.model.Section;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.SectionRepository;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.DiscussionIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.DiscussionNotFound;

import java.util.Optional;

@Component
public class DiscussionDtoMapper {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SectionRepository sectionRepository;

    public Discussion toModel(CreateDiscussionDto createDiscussionDto, String date) throws DiscussionNotFound, DiscussionIncorrect {

        Optional<User> userIdFound = userRepository.findById(createDiscussionDto.getUserId());
        Optional<Section> sectionIdFound = sectionRepository.findById(createDiscussionDto.getSectionId());


        if(userIdFound.isPresent() && sectionIdFound.isPresent()){
            User user = userIdFound.get();
            Section section = sectionIdFound.get();

            return new Discussion(null, date, createDiscussionDto.getTopic(), user, section);

        }else{
            throw new DiscussionIncorrect();
        }
    }

    public DiscussionDto toDto(Discussion discussion){

        return new DiscussionDto(discussion.getId(), discussion.getDate(), discussion.getTopic(), discussion.getUser().getId(), discussion.getSection().getId(),discussion.getUser().getLogin());
    }

}
