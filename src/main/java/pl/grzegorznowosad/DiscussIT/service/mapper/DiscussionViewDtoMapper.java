package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateDiscussionViewDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.DiscussionViewDto;
import pl.grzegorznowosad.DiscussIT.model.Discussion;
import pl.grzegorznowosad.DiscussIT.model.Section;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.SectionRepository;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.DiscussionIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;

import java.util.Optional;

@Component
public class DiscussionViewDtoMapper {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SectionRepository sectionRepository;

    public DiscussionViewDto toDto(Discussion discussion) throws UserNotFound {
        Optional<User> userById = userRepository.findById(discussion.getUser().getId());

        if(userById.isPresent()){
            User user = userById.get();

            return new DiscussionViewDto(discussion.getId(), discussion.getDate(), discussion.getTopic(), user.getLogin(),user.getId() ,discussion.getSection().getId());
        }else{
            throw new UserNotFound();
        }
    }

    public Discussion toModel(CreateDiscussionViewDto createDiscussionViewDto, String date) throws DiscussionIncorrect {
        Optional<User> userByLogin = userRepository.findByLogin(createDiscussionViewDto.getUserLogin());
        Optional<Section> sectionById = sectionRepository.findById(createDiscussionViewDto.getSectionId());

        if(userByLogin.isPresent() && sectionById.isPresent()){
            User user = userByLogin.get();
            Section section = sectionById.get();

            return new Discussion(null, date, createDiscussionViewDto.getTopic(), user, section);

        }else{
            throw new DiscussionIncorrect();
        }
    }
}
