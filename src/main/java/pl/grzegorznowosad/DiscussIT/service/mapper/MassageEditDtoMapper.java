package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.MessageEditDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.UpdateMessageDto;

@Component
public class MassageEditDtoMapper {

    public UpdateMessageDto toDto(MessageEditDto messageEditDto){
        return new UpdateMessageDto(messageEditDto.getText(),messageEditDto.getUserId(), messageEditDto.getDiscussionId());
    }
}
