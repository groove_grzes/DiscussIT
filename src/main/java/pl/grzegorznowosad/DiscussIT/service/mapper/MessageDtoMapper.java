package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateMessageDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.MessageDto;
import pl.grzegorznowosad.DiscussIT.model.Discussion;
import pl.grzegorznowosad.DiscussIT.model.Message;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.DiscussionRepository;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.MessageIncorrect;

import java.util.Optional;

@Component
public class MessageDtoMapper {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DiscussionRepository discussionRepository;

    public Message toModel(CreateMessageDto createMessageDto, String dateOfCreation) throws MessageIncorrect {
        Optional<User> userById = userRepository.findById(createMessageDto.getUserId());
        Optional<Discussion> discussionById = discussionRepository.findById(createMessageDto.getDiscussionId());

        if(discussionById.isPresent() && userById.isPresent()){
            User user = userById.get();
            Discussion discussion = discussionById.get();

            return new Message(null, createMessageDto.getText(), dateOfCreation, user, discussion);
        }else{
            throw new MessageIncorrect();
        }
    }

    public MessageDto toDto(Message message){
        return new MessageDto(message.getId(),message.getText(), message.getDate(), message.getUser().getId(), message.getDiscussion().getId());
    }

}
