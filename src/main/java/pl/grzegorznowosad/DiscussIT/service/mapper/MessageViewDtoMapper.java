package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateMessageViewDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.MessageDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.MessageViewDto;
import pl.grzegorznowosad.DiscussIT.model.Discussion;
import pl.grzegorznowosad.DiscussIT.model.Message;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.DiscussionRepository;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.MessageIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;
import pl.grzegorznowosad.DiscussIT.service.other.DateOfCreation;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.Optional;

@Component
public class MessageViewDtoMapper {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DiscussionRepository discussionRepository;

    public MessageViewDto toDto(Message message) throws UserNotFound, SQLException {
        Optional<User> userById = userRepository.findById(message.getUser().getId());

        if(userById.isPresent()){
            User user = userById.get();

            return new MessageViewDto(message.getId(), message.getText(), message.getDate(), user.getLogin(), user.getId() , message.getDiscussion().getId(), user.getRole().getRoleName(), getImageAsString(user.getAvatar()));
        }else{
            throw new UserNotFound();
        }
    }

    public Message toModel(CreateMessageViewDto createMessageViewDto, String date) throws UserNotFound {
        Optional<User> userByLogin = userRepository.findByLogin(createMessageViewDto.getUserLogin());
        Optional<Discussion> discussionById = discussionRepository.findById(createMessageViewDto.getDiscussionId());

        if(userByLogin.isPresent() && discussionById.isPresent()){
            User user = userByLogin.get();
            Discussion discussion = discussionById.get();

            return new Message(null, createMessageViewDto.getText(), date, user, discussion);

        }else{
            throw new UserNotFound();
        }
    }

    public String getImageAsString(Blob file) throws SQLException {
        int blobLength = (int) file.length();
        byte[] blobAsBytes = file.getBytes(1, blobLength);

        String obrazekString = Base64.encodeBase64String(blobAsBytes);

        return obrazekString;
    }
}
