package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.MessageViewDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.MessageViewFrontPageDto;
import pl.grzegorznowosad.DiscussIT.model.Discussion;
import pl.grzegorznowosad.DiscussIT.model.Message;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.DiscussionRepository;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;

import java.util.Optional;

@Component
public class MessageViewFrontPageDtoMapper {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DiscussionRepository discussionRepository;

    public MessageViewFrontPageDto toDto(Message message) throws UserNotFound {
        Optional<User> userById = userRepository.findById(message.getUser().getId());
        Optional<Discussion> discussionById = discussionRepository.findById(message.getDiscussion().getId());

        if(userById.isPresent() && discussionById.isPresent()){
            User user = userById.get();
            Discussion discussion = discussionById.get();

            return new MessageViewFrontPageDto(message.getId(), message.getText(), message.getDate(), user.getLogin(), user.getId() , message.getDiscussion().getId(),discussion.getTopic() );
        }else{
            throw new UserNotFound();
        }
    }
}
