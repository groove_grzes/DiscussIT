package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreatePrivateMessageDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.PrivateMessageDto;
import pl.grzegorznowosad.DiscussIT.model.PrivateMessage;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;

import java.util.Optional;

@Component
public class PrivateMessageDtoMapper {

    @Autowired
    private UserRepository userRepository;

    public PrivateMessage toModel(CreatePrivateMessageDto createPrivateMessageDto, String date) throws UserNotFound {
        Optional<User> senderById = userRepository.findById(createPrivateMessageDto.getSenderId());
        Optional<User> receiverById = userRepository.findById(createPrivateMessageDto.getReceiverId());

        if(senderById.isPresent() && receiverById.isPresent()){
            User sender = senderById.get();
            User receiver = receiverById.get();

            return new PrivateMessage(null,date, createPrivateMessageDto.getText(), "yes", sender, receiver);

        }else{
            throw new UserNotFound();
        }
    }

    public PrivateMessageDto toDto(PrivateMessage privateMessage){
        return new PrivateMessageDto(privateMessage.getId(), privateMessage.getDate(), privateMessage.getText(), privateMessage.getIsNew(), privateMessage.getSender().getId(), privateMessage.getReceiver().getId());
    }
}
