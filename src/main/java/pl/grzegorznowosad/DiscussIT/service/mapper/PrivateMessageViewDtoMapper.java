package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.*;
import pl.grzegorznowosad.DiscussIT.model.PrivateMessage;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;

import java.util.Optional;

@Component
public class PrivateMessageViewDtoMapper {

    @Autowired
    private UserRepository userRepository;

    public PrivateMessageViewDto toDto(PrivateMessage privateMessage) throws UserNotFound {
        Optional<User> senderById = userRepository.findById(privateMessage.getSender().getId());
        Optional<User> receiverById = userRepository.findById(privateMessage.getReceiver().getId());

        if(senderById.isPresent() && receiverById.isPresent()){
            User sender = senderById.get();
            User receiver = receiverById.get();

            return new PrivateMessageViewDto(privateMessage.getId(), privateMessage.getDate() ,privateMessage.getText(), privateMessage.getIsNew(), sender.getId(), sender.getLogin() ,receiver.getId(), receiver.getLogin() );

        }else{
            throw new UserNotFound();
        }
    }

    public CreatePrivateMessageDto toCreatePrivateMessageDto(CreatePrivateMessageViewDto createPrivateMessageViewDto){
        return new CreatePrivateMessageDto(createPrivateMessageViewDto.getText(), createPrivateMessageViewDto.getSenderId(), createPrivateMessageViewDto.getReceiverId());
    }

    public PrivateMessageViewDto toPrivateMessageViewDto(PrivateMessageDto privateMessageDto) throws UserNotFound {

        Optional<User> senderById = userRepository.findById(privateMessageDto.getSenderId());
        Optional<User> receiverById = userRepository.findById(privateMessageDto.getReceiverId());

        if(senderById.isPresent() && receiverById.isPresent()){
            User sender = senderById.get();
            User receiver = receiverById.get();

            return new PrivateMessageViewDto(privateMessageDto.getId(), privateMessageDto.getDate(), privateMessageDto.getText(), privateMessageDto.getIsNew(), privateMessageDto.getSenderId(), sender.getLogin(), privateMessageDto.getReceiverId(), receiver.getLogin());
        }else{
            throw new UserNotFound();
        }


    }
}
