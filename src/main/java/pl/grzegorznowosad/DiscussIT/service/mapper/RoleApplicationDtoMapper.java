package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateUpdateRoleApplication;
import pl.grzegorznowosad.DiscussIT.controller.dto.RoleApplicationDto;
import pl.grzegorznowosad.DiscussIT.model.RoleApplication;

@Component
public class RoleApplicationDtoMapper {

    public RoleApplication toModel(CreateUpdateRoleApplication createUpdateRoleApplication){
        return new RoleApplication(null, createUpdateRoleApplication.getRoleName(), null);
    }

    public RoleApplicationDto toDto(RoleApplication roleApplication){
        return new RoleApplicationDto(roleApplication.getId(), roleApplication.getRoleName());
    }

}
