package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateUpdateSectionDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.SectionDto;
import pl.grzegorznowosad.DiscussIT.model.Category;
import pl.grzegorznowosad.DiscussIT.model.Section;
import pl.grzegorznowosad.DiscussIT.repository.CategoryRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.CategoryNotFound;

import java.util.Optional;

@Component
public class SectionDtoMapper {

    @Autowired
    private CategoryRepository categoryRepository;

    public SectionDto toDto(Section section){
        return new SectionDto(section.getId(), section.getSectionTopic(), section.getCategory().getId());
    }

    public Section toModel(CreateUpdateSectionDto createUpdateSectionDto) throws CategoryNotFound {

        Optional<Category> byId = categoryRepository.findById(createUpdateSectionDto.getCategoryId());
        if(byId.isPresent()){
            Category category = byId.get();

            return new Section(null, createUpdateSectionDto.getSectionTopic(), category, null);
        }else{
            throw new CategoryNotFound();
        }


    }
}
