package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateUpdateShoutboxMessageDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.ShoutboxMessageDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.UserDto;
import pl.grzegorznowosad.DiscussIT.model.ShoutboxMessage;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.UserService;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;
import pl.grzegorznowosad.DiscussIT.service.other.DateOfCreation;

import java.util.Optional;

@Component
public class ShoutboxMessageDtoMapper {

    @Autowired
    private UserRepository userRepository;

    public ShoutboxMessage toModel(CreateUpdateShoutboxMessageDto createUpdateShoutboxMessageDto, String date) throws UserNotFound {

        Optional<User> userById = userRepository.findById(createUpdateShoutboxMessageDto.getUserId());

        if(userById.isPresent()){
            User user = userById.get();

            return new ShoutboxMessage(null, createUpdateShoutboxMessageDto.getText(), date, user);
        }else{
            throw new UserNotFound();
        }
    }

    public ShoutboxMessageDto toDto(ShoutboxMessage shoutboxMessage){
        return new ShoutboxMessageDto(shoutboxMessage.getId(), shoutboxMessage.getText(), shoutboxMessage.getDate(), shoutboxMessage.getUser().getId());
    }

}
