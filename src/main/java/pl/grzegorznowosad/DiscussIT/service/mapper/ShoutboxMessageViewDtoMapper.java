package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.ShoutboxMessageViewDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.UserDto;
import pl.grzegorznowosad.DiscussIT.model.ShoutboxMessage;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.UserService;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;

import java.util.Optional;

@Component
public class ShoutboxMessageViewDtoMapper {

    @Autowired
    private UserRepository userRepository;

    public ShoutboxMessageViewDto toDto(ShoutboxMessage shoutboxMessage) throws UserNotFound {
        Optional<User> userById = userRepository.findById(shoutboxMessage.getUser().getId());

        if(userById.isPresent()){
            User user = userById.get();

            return new ShoutboxMessageViewDto(shoutboxMessage.getId(), shoutboxMessage.getText(), shoutboxMessage.getDate(), user.getId(), user.getLogin());

        }else{
            throw new UserNotFound();
        }
    }
}
