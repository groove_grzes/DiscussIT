package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.UpdateUserDto;
import pl.grzegorznowosad.DiscussIT.model.User;

@Component
public class UpdateUserDtoMapper {

    public UpdateUserDto toDto(User user){
        return new UpdateUserDto(user.getPassword(), user.getFirstName(), user.getBirthYear(), user.getEmail(), user.getAvatar());
    }
}
