package pl.grzegorznowosad.DiscussIT.service.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grzegorznowosad.DiscussIT.controller.dto.RoleApplicationDto;
import pl.grzegorznowosad.DiscussIT.model.RoleApplication;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.controller.dto.UserCreateDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.UserDto;
import pl.grzegorznowosad.DiscussIT.repository.RoleApplicationRepository;
import pl.grzegorznowosad.DiscussIT.service.RoleApplicationService;
import pl.grzegorznowosad.DiscussIT.service.exception.RoleApplicationNotFound;

import java.util.Optional;

@Component
public class UserDtoMapper {
    public void setRoleApplicationRepository(RoleApplicationRepository roleApplicationRepository) {
        this.roleApplicationRepository = roleApplicationRepository;
    }

    @Autowired
    private RoleApplicationRepository roleApplicationRepository;

    public User toModel(UserCreateDto createDto) throws RoleApplicationNotFound {

        Optional<RoleApplication> roleUSER = roleApplicationRepository.findByRoleName("USER");

        if(roleUSER.isPresent()){
            RoleApplication roleApplicationUSER = roleUSER.get();

            return new User(null, createDto.getLogin(), createDto.getPassword(), createDto.getFirstName(), createDto.getBirthYear(),createDto.getEmail(), "yes", createDto.getAvatar(), null, roleApplicationUSER);
        }else{
            throw new RoleApplicationNotFound();
        }
    }

    public UserDto toDto(User user){
        return new UserDto(user.getId(), user.getLogin(), user.getFirstName(), user.getBirthYear(), user.getEmail(), user.getAvatar(), user.getIsActive(), user.getRole().getRoleName());
    }

}
