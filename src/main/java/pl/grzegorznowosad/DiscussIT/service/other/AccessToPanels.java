package pl.grzegorznowosad.DiscussIT.service.other;

import org.springframework.stereotype.Component;

@Component
public class AccessToPanels {

    public boolean canAccessToAdminPanelCheck(String role){
        if(role.equals("ADMIN") || role.equals("GOD")){
            return true;
        }else{
            return false;
        }
    }

    public boolean canAccessToGodPanelBecauseisGod(String role){
        if(role.equals("GOD")){
            return true;
        }else{
            return false;
        }
    }

}
