package pl.grzegorznowosad.DiscussIT.service.other;

import org.springframework.stereotype.Component;

@Component
public class CanDeleteThings {

    public boolean canUserDeleteThings(String role){
        if(role.equals("ROLE_MODERATOR") || role.equals("ROLE_ADMIN") || role.equals("ROLE_GOD")) return true;
        else return false;
    }
}
