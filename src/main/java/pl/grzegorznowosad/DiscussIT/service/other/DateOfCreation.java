package pl.grzegorznowosad.DiscussIT.service.other;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateOfCreation {

    private Date date = new Date();
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy , HH:mm:ss");

    public String getDate(){
        return sdf.format(date);
    }
}
