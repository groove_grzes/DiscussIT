package pl.grzegorznowosad.DiscussIT.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateDiscussionViewDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.*;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.service.*;
import pl.grzegorznowosad.DiscussIT.service.exception.*;
import pl.grzegorznowosad.DiscussIT.service.mapper.*;
import pl.grzegorznowosad.DiscussIT.service.other.AccessToPanels;
import pl.grzegorznowosad.DiscussIT.service.other.CanDeleteThings;

import javax.validation.Valid;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

@Controller
public class ViewController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SectionService sectionService;

    @Autowired
    private DiscussionService discussionService;

    @Autowired
    private DiscussionViewDtoMapper discussionViewDtoMapper;

    @Autowired
    private MessageService messageService;

    @Autowired
    private MessageViewDtoMapper messageViewDtoMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private MassageEditDtoMapper massageEditDtoMapper;

    @Autowired
    private ShoutboxService shoutboxService;

    @Autowired
    private PrivateMessageService privateMessageService;

    @Autowired
    private UpdateUserDtoMapper updateUserDtoMapper;

    @Autowired
    private CanDeleteThings canDeleteThings;

    @Autowired
    private PrivateMessageViewDtoMapper privateMessageViewDtoMapper;

    @Autowired
    private AccessToPanels accessToPanels;

    @RequestMapping({"/", "/home"})
    public ModelAndView toMain(Authentication authentication,
                               @RequestParam(required = false, name = "invalidShoutbox") String invalidShoutbox
    ) throws UserNotFound, CategoryNotFound, DiscussionNotFound, SectionNotFound, SQLException {
        ModelAndView mav = new ModelAndView("home");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();


            // nowy shoutbox
            CreateUpdateShoutboxMessageDto createUpdateShoutboxMessageDto = new CreateUpdateShoutboxMessageDto();
            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();
            createUpdateShoutboxMessageDto.setUserId(userLoggedId);
            mav.addObject("newshouboxMessage", createUpdateShoutboxMessageDto);
            mav.addObject("userId", userLoggedId);
            mav.addObject("isShoutboxInvalid", invalidShoutbox != null);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);

        }

        mav.addObject("userLogin", userLogin);

        List<CategoryDto> allCategories = categoryService.getAll();
        List<SectionDto> allSections = sectionService.getAll();

        mav.addObject("allCategories", allCategories);
        mav.addObject("allSections", allSections);

        // top 5 najnowszych watkow
        List<DiscussionDto> top5DescDiscussions = discussionService.findTop5Desc();
        mav.addObject("top5Discussions",top5DescDiscussions);


        // top 10 shoutboxow
        List<ShoutboxMessageViewDto> top10ShoutboxMessagesDesc = shoutboxService.getTop10Desc();
        mav.addObject("shoutboxMessages", top10ShoutboxMessagesDesc);


        // ostatnie 10 wiadomosci
        List<MessageViewFrontPageDto> last10Messages = messageService.getLast10Messages();
        mav.addObject("lastMessages", last10Messages);


        // pobranie wszystkich statystyk
        Map<String, Integer> mapOfStatistics = new HashMap<>();
        List<UserDto> allUsers = userService.getAll();
        mapOfStatistics.put("numberOfAllUsers", allUsers.size());

        List<DiscussionDto> allDiscussions = discussionService.getAll();
        mapOfStatistics.put("numerOfAllDiscussions", allDiscussions.size());

        List<MessageDto> allMessages = messageService.getAll();
        mapOfStatistics.put("numberOfAllMessages", allMessages.size());

        // obecnie zalogowani
        mapOfStatistics.put("numberOfCurrentLoggedUsers", new Integer(0));
        mav.addObject("mapOfStatistics", mapOfStatistics);

        // dane sekcji na front page
        Map<Integer, SectionNumbersFrontPage> dataForSectionsForFrontPage = sectionService.getDataForSectionsForFrontPage();
        mav.addObject("sectionNumbersToFront", dataForSectionsForFrontPage);

        UserDto latestUser = userService.getLatestUser();
        mav.addObject("lastNewUser", latestUser);

        return mav;
    }

    @RequestMapping("/home2")
    public ModelAndView toMain2(Authentication authentication) throws UserNotFound, CategoryNotFound, DiscussionNotFound, SectionNotFound, SQLException {
        ModelAndView mav = new ModelAndView("home_2");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();


            // nowy shoutbox
            CreateUpdateShoutboxMessageDto createUpdateShoutboxMessageDto = new CreateUpdateShoutboxMessageDto();
            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();
            createUpdateShoutboxMessageDto.setUserId(userLoggedId);
            mav.addObject("newshouboxMessage", createUpdateShoutboxMessageDto);
            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);

        }

        mav.addObject("userLogin", userLogin);

        List<CategoryDto> allCategories = categoryService.getAll();
        List<SectionDto> allSections = sectionService.getAll();

        mav.addObject("allCategories", allCategories);
        mav.addObject("allSections", allSections);

        // top 5 najnowszych watkow
        List<DiscussionDto> top5DescDiscussions = discussionService.findTop5Desc();
        mav.addObject("top5Discussions",top5DescDiscussions);


        // top 10 shoutboxow
        List<ShoutboxMessageViewDto> top10ShoutboxMessagesDesc = shoutboxService.getTop10Desc();
        mav.addObject("shoutboxMessages", top10ShoutboxMessagesDesc);


        // ostatnie 10 wiadomosci
        List<MessageViewFrontPageDto> last10Messages = messageService.getLast10Messages();
        mav.addObject("lastMessages", last10Messages);


        // pobranie wszystkich statystyk
        Map<String, Integer> mapOfStatistics = new HashMap<>();
        List<UserDto> allUsers = userService.getAll();
        mapOfStatistics.put("numberOfAllUsers", allUsers.size());

        List<DiscussionDto> allDiscussions = discussionService.getAll();
        mapOfStatistics.put("numerOfAllDiscussions", allDiscussions.size());

        List<MessageDto> allMessages = messageService.getAll();
        mapOfStatistics.put("numberOfAllMessages", allMessages.size());

        // obecnie zalogowani
        mapOfStatistics.put("numberOfCurrentLoggedUsers", new Integer(0));
        mav.addObject("mapOfStatistics", mapOfStatistics);

        // dane sekcji na front page
        Map<Integer, SectionNumbersFrontPage> dataForSectionsForFrontPage = sectionService.getDataForSectionsForFrontPage();
        mav.addObject("sectionNumbersToFront", dataForSectionsForFrontPage);

        UserDto latestUser = userService.getLatestUser();
        mav.addObject("lastNewUser", latestUser);

        return mav;
    }


    @RequestMapping("/section/{id}")
    public ModelAndView toSection(@PathVariable Integer id, Authentication authentication, @RequestParam(required = false, name = "invalidShoutbox") String invalidShoutbox) throws SectionNotFound, CategoryNotFound, UserNotFound, DiscussionNotFound, SQLException {
        ModelAndView mav = new ModelAndView("section");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
            mav.addObject("isShoutboxInvalid", invalidShoutbox != null);
        }
        mav.addObject("userLogin", userLogin);


        List<DiscussionViewDto> discussionsBySectionId = discussionService.getDiscussionsBySectionId(id);

        Map<Integer, Integer> numberOfMessagesInDiscussions = new HashMap<>();
        Map<Integer, MessageViewDto> lastMessageInDiscussion = new HashMap<>();

        for(DiscussionViewDto discussionViewDto : discussionsBySectionId){
            List<MessageViewDto> messagesByDiscussionId = messageService.getMessagesByDiscussionId(discussionViewDto.getId());

            numberOfMessagesInDiscussions.put(discussionViewDto.getId(), messagesByDiscussionId.size());

            if(messagesByDiscussionId.size() != 0){
                lastMessageInDiscussion.put(discussionViewDto.getId(), messagesByDiscussionId.get(messagesByDiscussionId.size() - 1));
            }else{
                lastMessageInDiscussion.put(discussionViewDto.getId(), null);
            }


        }

        mav.addObject("mapOfDiscussionsNumber", numberOfMessagesInDiscussions);
        mav.addObject("mapOfLastMessageInDiscussion", lastMessageInDiscussion);

        mav.addObject("discussionsBySectionId", discussionsBySectionId);

        SectionDto sectionById = sectionService.getById(id);
        mav.addObject("sectionDetails",sectionById);

        CategoryDto categoryById = categoryService.getById(sectionById.getCategoryId());
        mav.addObject("categoryDetails", categoryById);

        UserDto userByLogin = userService.getByLogin(authentication.getName());
        mav.addObject("userId", userByLogin.getId());
        mav.addObject("userLogin", userByLogin.getLogin());

        CreateDiscussionViewDto createDiscussionViewDto = new CreateDiscussionViewDto();
        createDiscussionViewDto.setSectionId(id);
        createDiscussionViewDto.setUserLogin(userByLogin.getLogin());

        mav.addObject("newDiscussion", createDiscussionViewDto);


        return mav;
    }

    @PostMapping("/section/addDiscussion")
    public String addDiscussion(@Valid @ModelAttribute(name = "newDiscussion") CreateDiscussionViewDto createDiscussionViewDto, BindingResult bindingResult) throws DiscussionAlreadyExist, DiscussionIncorrect, MessageIncorrect {
        Integer sectionId = createDiscussionViewDto.getSectionId();

        if(!bindingResult.hasErrors()){
            discussionService.saveNewDiscussion(createDiscussionViewDto);
            return "redirect:/section/" + sectionId;

        }

        return "redirect:/section/" + sectionId + "?invalidShoutbox";
    }


//    @GetMapping("/discussion/{id}")
//    public ModelAndView toDiscussion(@PathVariable Integer id, Authentication authentication) throws DiscussionNotFound, SectionNotFound, CategoryNotFound, UserNotFound, SQLException {
//        ModelAndView mav = new ModelAndView("discussion");
//
//        List<MessageViewDto> messagesByDiscussionId = messageService.getMessagesByDiscussionId(id);
//
//        mav.addObject("allMessagesViewDto", messagesByDiscussionId);
//
//        Map<Integer, Integer> mapNumperOfMessagesPerUser = messageService.numberOfMessagesByUser();
//
//        mav.addObject("numberOfMessagesPerUser", mapNumperOfMessagesPerUser);
//
//        mav.addObject("lastMessage", messagesByDiscussionId.get(messagesByDiscussionId.size() - 1));
//
//        CreateMessageViewDto createMessageViewDtoDto = new CreateMessageViewDto();
//        createMessageViewDtoDto.setDiscussionId(id);
//        createMessageViewDtoDto.setUserLogin(authentication.getName());
//
//        mav.addObject("newMessageEntry", createMessageViewDtoDto);
//
//        DiscussionDto discussionById = discussionService.getById(id);
//        mav.addObject("discussionDetails", discussionById);
//
//        SectionDto sectionDetails = sectionService.getById(discussionById.getSectionId());
//        mav.addObject("sectionDetails", sectionDetails);
//
//        CategoryDto categoryById = categoryService.getById(sectionDetails.getCategoryId());
//        mav.addObject("categoryDetails", categoryById);
//
//        mav.addObject("userLogin", authentication.getName());
//
//        Object [] objects = authentication.getAuthorities().toArray();
//        String role = objects[0].toString();
//        mav.addObject("userRole", role);
//
//        boolean canDelete = canDeleteThings.canUserDeleteThings(role);
//
//        mav.addObject("canUserDeleteThing", canDelete);
//
//        UserDto userByLogin = userService.getByLogin(authentication.getName());
//        Integer userId = userByLogin.getId();
//        mav.addObject("userId", userId);
//
//
//        return mav;
//    }

    @GetMapping("/discussion/{id}")
    public ModelAndView toDiscussion(@PathVariable Integer id, Authentication authentication, @RequestParam(required = false, name = "invalidShoutbox") String invalidShoutbox) throws DiscussionNotFound, SectionNotFound, CategoryNotFound, UserNotFound, SQLException {
        ModelAndView mav = new ModelAndView("discussion");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
            mav.addObject("isShoutboxInvalid", invalidShoutbox != null);
        }

        mav.addObject("userLogin", userLogin);


        List<MessageViewDto> messagesByDiscussionId = messageService.getMessagesByDiscussionId(id);

        mav.addObject("allMessagesViewDto", messagesByDiscussionId);

        Map<Integer, Integer> mapNumperOfMessagesPerUser = messageService.numberOfMessagesByUser();

        mav.addObject("numberOfMessagesPerUser", mapNumperOfMessagesPerUser);


        if (messagesByDiscussionId.size() != 0){
            mav.addObject("lastMessage", messagesByDiscussionId.get(messagesByDiscussionId.size() - 1));
        }else{
            mav.addObject("lastMessage", null);
        }



        CreateMessageViewDto createMessageViewDtoDto = new CreateMessageViewDto();
        createMessageViewDtoDto.setDiscussionId(id);
        createMessageViewDtoDto.setUserLogin(authentication.getName());

        mav.addObject("newMessageEntry", createMessageViewDtoDto);

        DiscussionDto discussionById = discussionService.getById(id);
        mav.addObject("discussionDetails", discussionById);

        SectionDto sectionDetails = sectionService.getById(discussionById.getSectionId());
        mav.addObject("sectionDetails", sectionDetails);

        CategoryDto categoryById = categoryService.getById(sectionDetails.getCategoryId());
        mav.addObject("categoryDetails", categoryById);

        mav.addObject("userLogin", authentication.getName());

        Object [] objects = authentication.getAuthorities().toArray();
        String role = objects[0].toString();
        mav.addObject("userRole", role);

        boolean canDelete = canDeleteThings.canUserDeleteThings(role);

        mav.addObject("canUserDeleteThing", canDelete);

        UserDto userByLogin = userService.getByLogin(authentication.getName());
        Integer userId = userByLogin.getId();
        mav.addObject("userId", userId);


        return mav;
    }


    @PostMapping("/discussion/addMessage")
    public String addMessage(@Valid @ModelAttribute(name = "newMessageEntry") CreateMessageViewDto createMessageViewDto, BindingResult bindingResult) throws MessageIncorrect, MessageAlreadyExist, UserNotFound {

        Integer discussionId = createMessageViewDto.getDiscussionId();

        if(!bindingResult.hasErrors()){
            messageService.createMessageFromView(createMessageViewDto);
            return "redirect:/discussion/" + discussionId;
        }


        return "redirect:/discussion/" + discussionId + "?invalidShoutbox";
    }

    @GetMapping("/category/{id}")
    public ModelAndView getCategory(@PathVariable Integer id) throws CategoryNotFound {
        ModelAndView mav = new ModelAndView("category");

        CategoryDto categoryById = categoryService.getById(id);
        mav.addObject("categoryDetails", categoryById);

        List<SectionDto> sectionListByCategoryId = sectionService.getSectionByCategoryId(id);
        mav.addObject("allSections", sectionListByCategoryId);

        return mav;
    }

    @GetMapping("/message/delete/{id}")
    public String deleteMessage(@PathVariable Integer id) throws MessageNotFound {
        MessageDto messageById = messageService.getById(id);

        MessageDto messageDtoDeleted = messageService.deleteMessage(messageById.getId());

        return "redirect:/discussion/" + messageDtoDeleted.getDiscussionId();
    }

    @GetMapping("/message/edit/{id}")
    public ModelAndView editMessage(@PathVariable Integer id, Authentication authentication) throws MessageNotFound, UserNotFound {
        ModelAndView mav = new ModelAndView("editMessage");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
        }
        mav.addObject("userLogin", userLogin);

        MessageEditDto messageEditDto = new MessageEditDto();

        MessageDto messageById = messageService.getById(id);

        messageEditDto.setText(messageById.getText());
        messageEditDto.setUserId(messageById.getUserId());
        messageEditDto.setDiscussionId(messageById.getDiscussionId());
        messageEditDto.setId(messageById.getId());

        mav.addObject("editMessage", messageById);

        return mav;
    }

    @PostMapping("/message/edit")
    public String editMessageFinal(@Valid @ModelAttribute(name = "editMessage") MessageEditDto messageEditDto, BindingResult bindingResult) throws MessageIncorrect, MessageNotFound {

        Integer discussionId = messageEditDto.getDiscussionId();
        Integer messsageId = messageEditDto.getId();

        if(bindingResult.hasErrors()){
            return "redirect:/message/edit/" + messsageId;
        }

        MessageDto messageDto = messageService.updateMessage(massageEditDtoMapper.toDto(messageEditDto), messageEditDto.getId());

        return "redirect:/discussion/" + discussionId;
    }

    @PostMapping("/shoutbox/addMessage")
    public String addShoutboxMessage(@Valid @ModelAttribute(name = "newshouboxMessage") CreateUpdateShoutboxMessageDto createUpdateShoutboxMessageDto, BindingResult bindingResult, Authentication authentication) throws UserNotFound, ShoutboxMessageIncorrect, SQLException, SectionNotFound, DiscussionNotFound, CategoryNotFound {

        if(!bindingResult.hasErrors()){
            shoutboxService.create(createUpdateShoutboxMessageDto);
            return "redirect:/";
        }

        return "redirect:/?invalidShoutbox";
    }

    @GetMapping("/PrivateMessages/")
    public ModelAndView toPrivateMessages(Authentication authentication) throws UserNotFound {
        ModelAndView mav = new ModelAndView("privatemessages");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
        }
        mav.addObject("userLogin", userLogin);

        UserDto userById = userService.getByLogin(authentication.getName());
        Integer userId = userById.getId();

        List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userId);
        mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
        List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userId);
        mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
        List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userId);
        mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

        return mav;
    }

    @GetMapping("/PrivateMessages/send")
    public ModelAndView toPMsend(Authentication authentication) throws UserNotFound {

        ModelAndView mav = new ModelAndView("PMsSend");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
        }
        mav.addObject("userLogin", userLogin);

        UserDto userByLogin = userService.getByLogin(authentication.getName());
        List<PrivateMessageViewDto> allBySender = privateMessageService.getAllBySender(userByLogin.getId());


        mav.addObject("allSendPMs", allBySender);

        return mav;
    }

    @GetMapping("/PrivateMessages/received")
    public ModelAndView toPMreceived(Authentication authentication) throws UserNotFound {
        ModelAndView mav = new ModelAndView("PMsReceived");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
        }
        mav.addObject("userLogin", userLogin);

        UserDto userByLogin = userService.getByLogin(authentication.getName());
        List<PrivateMessageViewDto> allByReceiver = privateMessageService.getAllByReceiver(userByLogin.getId());


        mav.addObject("allReceivedPMs", allByReceiver);

        List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userByLogin.getId());
        List<PrivateMessageViewDto> allNewPrivateMessagesViewForUser = new ArrayList<>();
        for(PrivateMessageDto pmNew : allNewPrivateMessagesForUser){
            allNewPrivateMessagesViewForUser.add(privateMessageViewDtoMapper.toPrivateMessageViewDto(pmNew));
        }
        mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesViewForUser);

        List<PrivateMessageDto> allOldPrivateMessagesForUser = privateMessageService.getAllOldPrivateMessagesForUser(userByLogin.getId());
        List<PrivateMessageViewDto> allOldPrivateMessagesViewForUser = new ArrayList<>();
        for(PrivateMessageDto pmOld : allOldPrivateMessagesForUser){
            allOldPrivateMessagesViewForUser.add(privateMessageViewDtoMapper.toPrivateMessageViewDto(pmOld));
        }
        mav.addObject("oldPrivateMessagesForUser", allOldPrivateMessagesViewForUser);

        return mav;
    }

    @GetMapping("/PrivateMessages/read/{id}")
    public ModelAndView toPMreceived(@PathVariable Integer id, Authentication authentication) throws UserNotFound, MessageNotFound, PrivateMessageNotFound {

        privateMessageService.markAsRead(id);

        ModelAndView mav = new ModelAndView("readMessage");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);

            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
        }
        mav.addObject("userLogin", userLogin);

        PrivateMessageDto privateMessageById = privateMessageService.getById(id);

        mav.addObject("message", privateMessageById);

        return mav;
    }

    @GetMapping("/PrivateMessages/unread/{id}")
    public String toPMUnread(@PathVariable Integer id,Authentication authentication) throws UserNotFound, MessageNotFound, PrivateMessageNotFound {

        privateMessageService.markAsUnread(id);

//        ModelAndView mav = new ModelAndView("readMessage");

//        PrivateMessageDto privateMessageById = privateMessageService.getById(id);

//        mav.addObject("message", privateMessageById);

        return "redirect:/PrivateMessages/received";
    }

    @GetMapping("/PrivateMessages/delete/{id}")
    public String deleteMessageView(@PathVariable Integer id) throws PrivateMessageNotFound {
        PrivateMessageDto delete = privateMessageService.delete(id);

        return "redirect:/PrivateMessages/received";
    }

    @GetMapping("/PrivateMessages/write")
    public ModelAndView toWritePM(Authentication authentication) throws UserNotFound {

        ModelAndView mav = new ModelAndView("PMwrite");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
        }
        mav.addObject("userLogin", userLogin);

        CreatePrivateMessageViewDto createPrivateMessageViewDto = new CreatePrivateMessageViewDto();

        UserDto userByLogin = userService.getByLogin(authentication.getName());
        createPrivateMessageViewDto.setSenderId(userByLogin.getId());
        createPrivateMessageViewDto.setSenderLogin(userByLogin.getLogin());
        createPrivateMessageViewDto.setReceiverId(new Integer(0));

        mav.addObject("newPM", createPrivateMessageViewDto);

        List<UserDto> allUsersDto = userService.getAll();
        List<UserDataDtoForDataListForm> allUsersDtoUserLogin = new ArrayList<>();

        allUsersDto.forEach(x -> {
            allUsersDtoUserLogin.add(new UserDataDtoForDataListForm(x.getLogin()));
        });

        mav.addObject("allUsersLogins", allUsersDtoUserLogin);

        return mav;
    }

    @PostMapping("/PrivateMessages/write")
    public String addNewPM(@ModelAttribute(name = "newPM") CreatePrivateMessageViewDto createPrivateMessageViewDto) throws UserNotFound, PrivateMessageDataIncorrect {

        String receiverLogin = createPrivateMessageViewDto.getReceiverLogin();

        UserDto userById = userService.getByLogin(receiverLogin);

        createPrivateMessageViewDto.setReceiverId(userById.getId());
        createPrivateMessageViewDto.setReceiverLogin(userById.getLogin());

        privateMessageService.create(privateMessageViewDtoMapper.toCreatePrivateMessageDto(createPrivateMessageViewDto));

        return "redirect:/PrivateMessages/";
    }

    @GetMapping("/PrivateMessages/write/{id}")
    public ModelAndView toWritePMForUserDefined(Authentication authentication, @PathVariable Integer id) throws UserNotFound {

        ModelAndView mav = new ModelAndView("PMwriteWithUserDefined");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
        }
        mav.addObject("userLogin", userLogin);

        CreatePrivateMessageViewDto createPrivateMessageViewDto = new CreatePrivateMessageViewDto();

        UserDto receiverById = userService.getById(id);
        UserDto senderByLogin = userService.getByLogin(authentication.getName());

        createPrivateMessageViewDto.setSenderId(senderByLogin.getId());
        createPrivateMessageViewDto.setSenderLogin(senderByLogin.getLogin());
        createPrivateMessageViewDto.setReceiverId(receiverById.getId());
        createPrivateMessageViewDto.setSenderLogin(receiverById.getLogin());

        mav.addObject("newPM", createPrivateMessageViewDto);

        return mav;
    }

    @PostMapping("/PrivateMessages/write/{id}")
    public String addNewPM(@Valid @ModelAttribute(name = "newPM") CreatePrivateMessageViewDto createPrivateMessageViewDto, BindingResult bindingResult, @PathVariable Integer id) throws UserNotFound, PrivateMessageDataIncorrect {

        if(bindingResult.hasErrors()){
            return "redirect:/PrivateMessages/write/" + id;
        }

        UserDto userById = userService.getById(id);

        createPrivateMessageViewDto.setReceiverId(userById.getId());
        createPrivateMessageViewDto.setReceiverLogin(userById.getLogin());

        privateMessageService.create(privateMessageViewDtoMapper.toCreatePrivateMessageDto(createPrivateMessageViewDto));

        return "redirect:/PrivateMessages/";
    }

    @GetMapping("/register")
    public ModelAndView toRegister(Authentication authentication) throws UserNotFound {
        ModelAndView mav = new ModelAndView("register");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
        }
        mav.addObject("userLogin", userLogin);

        UserCreateDtoWithFile userCreateDtoWithFile = new UserCreateDtoWithFile();
        mav.addObject("newUserObject", userCreateDtoWithFile);

        return mav;
    }

    @PostMapping("/register")
    public String addUser(@Valid @ModelAttribute(name = "newUserObject")UserCreateDtoWithFile userCreateDtoWithFile, BindingResult bindingResult) throws LoginIncorrect, UserAlreadyExist, RoleApplicationNotFound, IOException, SQLException {

        if(userCreateDtoWithFile.getAvatar().getSize() == 0){
            bindingResult.addError(new FieldError("userCreateDtoWithFile", "avatar", "May not be null"));
        }

        if(bindingResult.hasErrors()){
            return "register";
        }


        UserCreateDto userCreateDto = new UserCreateDto();
        userCreateDto.setLogin(userCreateDtoWithFile.getLogin());
        userCreateDto.setPassword(userCreateDtoWithFile.getPassword());
        userCreateDto.setFirstName(userCreateDtoWithFile.getFirstName());
        userCreateDto.setBirthYear(userCreateDtoWithFile.getBirthYear());
        userCreateDto.setEmail(userCreateDtoWithFile.getEmail());
        userCreateDto.setAvatar(userCreateDtoWithFile.getAvatar().getBytes());

        userService.createUser(userCreateDto);

        return "redirect:/";
    }

    @GetMapping("/users")
    public ModelAndView toListOfUsers(Authentication authentication) throws UserNotFound {
        ModelAndView mav = new ModelAndView("allUsers");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
        }
        mav.addObject("userLogin", userLogin);

        List<UserDto> allUsers = userService.getAll();
        mav.addObject("listOfAllUsers", allUsers);

        return mav;
    }

    @GetMapping("/user/show")
    public ModelAndView toMyProfile(Authentication authentication) throws UserNotFound {
        ModelAndView mav = new ModelAndView("myProfile");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
        }
        mav.addObject("userLogin", userLogin);

        UserDto userByLogin = userService.getByLogin(authentication.getName());

        mav.addObject("userProfile", userByLogin);

        return mav;
    }

    @GetMapping("/user/{id}")
    public ModelAndView toProfile(@PathVariable Integer id, Authentication authentication) throws UserNotFound {
        ModelAndView mav = new ModelAndView("myProfile");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
        }
        mav.addObject("userLogin", userLogin);

        UserDto userByLogin = userService.getById(id);

        mav.addObject("userProfile", userByLogin);

        Integer numberOfMessagesByUser = messageService.numberOfMessagesByAuthor(id);
        mav.addObject("numberOfMessages", numberOfMessagesByUser);

        Integer numberOfDiscussionsByUser = discussionService.numberOfDiscussionsByUser(id);
        mav.addObject("numberOfDiscussions", numberOfDiscussionsByUser);

        return mav;
    }


    @GetMapping("/user/edit")
    public ModelAndView toEditProfile(Authentication authentication) throws UserNotFound {
        ModelAndView mav = new ModelAndView("editProfile");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
        }
        mav.addObject("userLogin", userLogin);

        User user = userService.getFullUser(authentication.getName());
        UpdateUserDtoWithFile updateUserDtoWithFile = new UpdateUserDtoWithFile();
        updateUserDtoWithFile.setFirstName(user.getFirstName());
        updateUserDtoWithFile.setBirthYear(user.getBirthYear());
        updateUserDtoWithFile.setEmail(user.getEmail());

        mav.addObject("userToEdit", updateUserDtoWithFile);

        return mav;
    }

    @PostMapping("/user/edit")
    public String editProfile(@Valid @ModelAttribute(name = "editProfile") UpdateUserDtoWithFile updateUserDtoWithFile, BindingResult bindingResult, Authentication authentication) throws UserNotFound, IOException, SQLException {

        if(bindingResult.hasErrors()){
            return "redirect:/user/edit";
        }

        UserDto userByLogin = userService.getByLogin(authentication.getName());

        UpdateUserDto updateUserDto = new UpdateUserDto();
//        updateUserDto.setPassword(updateUserDtoWithFile.getPassword());
        updateUserDto.setFirstName(updateUserDtoWithFile.getFirstName());
        updateUserDto.setBirthYear(updateUserDtoWithFile.getBirthYear());
        updateUserDto.setEmail(updateUserDtoWithFile.getEmail());
        updateUserDto.setAvatar(updateUserDtoWithFile.getAvatar().getBytes());

        userService.updateUser(userByLogin.getId(), updateUserDto);

        return "redirect:/";
    }

    @GetMapping("/admin")
    public ModelAndView toAdminPanel(Authentication authentication) throws UserNotFound {
        ModelAndView mav = new ModelAndView("admin");

        String userLogin = null;

        if(authentication != null){
            userLogin = authentication.getName();

            UserDto userByLogin = userService.getByLogin(userLogin);
            Integer userLoggedId = userByLogin.getId();

            mav.addObject("userId", userLoggedId);


            // prywatne wiadomosci
            List<PrivateMessageViewDto> allPrivateMessagesByReceiver = privateMessageService.getAllByReceiver(userLoggedId);
            mav.addObject("privateMessagesReceived", allPrivateMessagesByReceiver);
            List<PrivateMessageViewDto> allPrivateMessagesBySender = privateMessageService.getAllBySender(userLoggedId);
            mav.addObject("privateMessagesSended", allPrivateMessagesBySender);
            Integer numberOfUserMessages = allPrivateMessagesByReceiver.size() + allPrivateMessagesByReceiver.size();
            mav.addObject("numberOfUserMessages", numberOfUserMessages);
            List<PrivateMessageDto> allNewPrivateMessagesForUser = privateMessageService.getAllNewPrivateMessagesForUser(userLoggedId);
            mav.addObject("newPrivateMessagesForUser", allNewPrivateMessagesForUser);

            boolean canGoToAdminPanel = accessToPanels.canAccessToAdminPanelCheck(userByLogin.getRole());
            mav.addObject("canGoToAdmin", canGoToAdminPanel);

            boolean canGoToGodPanel = accessToPanels.canAccessToGodPanelBecauseisGod(userByLogin.getRole());
            mav.addObject("canGoToGod", canGoToGodPanel);
        }
        mav.addObject("userLogin", userLogin);

        Object[] objects = authentication.getAuthorities().toArray();
        String userRole = objects[0].toString();

        mav.addObject("userRole", userRole.substring(5));

        DeleteUserDto deleteUserDto = new DeleteUserDto();
        mav.addObject("userToDelete", deleteUserDto);

        ChangeRoleUserDto changeRoleUserDto = new ChangeRoleUserDto();
        mav.addObject("userChangeRole", changeRoleUserDto);

        String actionAlertUserDeleted = null;
        mav.addObject("actionAlertDeleted", actionAlertUserDeleted);

        List<UserDto> allUsersDto = userService.getAll();
        List<UserDataDtoForDataListForm> allUsersDtoUserLogin = new ArrayList<>();

        allUsersDto.forEach(x -> {
            allUsersDtoUserLogin.add(new UserDataDtoForDataListForm(x.getLogin()));
        });

        mav.addObject("allUsersLogins", allUsersDtoUserLogin);

        return mav;
    }

    @PostMapping("/admin/deleteuser")
    public String toDeleteUser(@ModelAttribute(name = "userToDelete") DeleteUserDto deleteUserDto) throws UserNotFound {

        UserDto userByLogin = userService.getByLogin(deleteUserDto.getUserLogin());
        userService.deleteUser(userByLogin.getId());

        return "redirect:/admin";
    }

    @PostMapping("/admin/setrole")
    public String toSetRoleForUser(@ModelAttribute(name = "userChangeRole") ChangeRoleUserDto changeRoleUserDto) throws UserNotFound, RoleApplicationNotFound {

        userService.setNewRole(changeRoleUserDto.getUserLogin(), changeRoleUserDto.getNewUserRole());

        return "redirect:/admin";
    }

    @GetMapping("/accessDenied")
    public ModelAndView toAccessDenied(){
        ModelAndView mav = new ModelAndView("accessDenied");

        return mav;
    }

    @GetMapping("/clean")
    public ModelAndView toClean(Authentication authentication){
        ModelAndView mav = new ModelAndView("clean");

        return mav;
    }
}
