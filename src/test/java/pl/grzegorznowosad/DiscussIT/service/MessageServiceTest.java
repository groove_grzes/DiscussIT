package pl.grzegorznowosad.DiscussIT.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateMessageDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.CreateMessageViewDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.MessageDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.UserDto;
import pl.grzegorznowosad.DiscussIT.model.Discussion;
import pl.grzegorznowosad.DiscussIT.model.Message;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.DiscussionRepository;
import pl.grzegorznowosad.DiscussIT.repository.MessageRepository;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.MessageAlreadyExist;
import pl.grzegorznowosad.DiscussIT.service.exception.MessageIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.MessageNotFound;
import pl.grzegorznowosad.DiscussIT.service.exception.UserNotFound;
import pl.grzegorznowosad.DiscussIT.service.mapper.MessageDtoMapper;
import pl.grzegorznowosad.DiscussIT.service.mapper.MessageViewDtoMapper;
import pl.grzegorznowosad.DiscussIT.service.other.DateOfCreation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private DiscussionRepository discussionRepository;

    @Mock
    private MessageRepository messageRepository;

    @Mock
    private MessageDtoMapper messageDtoMapper;
    @Mock
    private MessageViewDtoMapper messageViewDtoMapper;

    @InjectMocks
    private MessageService messageService;

    @Captor
    private ArgumentCaptor<Message> argumentCaptor;

    // 1 - test checks if message service correctly return empty list
    @Test
    public void getAll_getEmptyList(){

        // given
        when(messageRepository.findAll()).thenReturn(new ArrayList<>());

        // when
        List<MessageDto> allMessages = messageService.getAll();

        // then
        assertEquals(allMessages.size(), 0);
    }

    // 2 - test checks if message service throws exception if we delete nonexisting message

    @Test(expected = MessageNotFound.class)
    public void deleteMessage_checkIfThrowsException() throws MessageNotFound {
        // given
        Optional<Message> emptyOptional = Optional.empty();
        when(messageRepository.findById(11)).thenReturn(emptyOptional);

        // when
        messageService.deleteMessage(11);
    }

    // 3 - test checks if method save() run on messsage repository properly

    @Test
    public void createMessage_checkIfSavedInRepositoryIsThisSameAsInput() throws MessageIncorrect, MessageAlreadyExist, UserNotFound {

        // given
        Message messageSaved = new Message();
        messageSaved.setDiscussion(new Discussion());

        CreateMessageViewDto createMessageViewDto = new CreateMessageViewDto("example test", "admin", 1);

        when(userRepository.findById(1)).thenReturn(Optional.of(new User()));


        Message value = new Message();
        when(messageViewDtoMapper.toModel(any(), any())).thenReturn(value);
        when(messageRepository.save(argumentCaptor.capture())).thenReturn(messageSaved);

        value.setText("example test");

        // when
        messageService.createMessageFromView(createMessageViewDto);

        // then
        Message message = argumentCaptor.getValue();
        assertEquals("example test", message.getText());


    }
}
