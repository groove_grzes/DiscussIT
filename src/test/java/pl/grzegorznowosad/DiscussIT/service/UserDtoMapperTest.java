package pl.grzegorznowosad.DiscussIT.service;

import com.mysql.jdbc.Blob;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.grzegorznowosad.DiscussIT.controller.dto.UserCreateDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.UserDto;
import pl.grzegorznowosad.DiscussIT.model.RoleApplication;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.RoleApplicationRepository;
import pl.grzegorznowosad.DiscussIT.service.exception.RoleApplicationNotFound;
import pl.grzegorznowosad.DiscussIT.service.mapper.UserDtoMapper;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDtoMapperTest {

    @Mock
    private RoleApplicationRepository roleApplicationRepository;

    @InjectMocks
    UserDtoMapper userDtoMapper = new UserDtoMapper();

    @Test
    public void toDto_shouldReturnCorrectDto(){

        // given
        User user = new User(123, "jan", "password123", "Jan", 1980, "mail@mail.com", "yes", null, new ArrayList<>(), new RoleApplication());

        // when
        UserDto userDto = userDtoMapper.toDto(user);

        // then
        assertEquals("jan", userDto.getLogin());
    }

    @Test
    public void toModel_shouldReturnCorrectModel() throws RoleApplicationNotFound {

        // given
//        UserDtoMapper userDtoMapper = new UserDtoMapper();
        when(roleApplicationRepository.findByRoleName(any())).thenReturn(Optional.of(new RoleApplication()));

        UserCreateDto userCreateDto = new UserCreateDto("jan", "password123", "Jan", 1980, "mail@mail.com", null);

        // when
        User user = userDtoMapper.toModel(userCreateDto);

        // then
        assertEquals("jan", user.getLogin());
    }

}
