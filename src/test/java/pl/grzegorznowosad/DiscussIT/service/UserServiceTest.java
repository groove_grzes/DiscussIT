package pl.grzegorznowosad.DiscussIT.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.grzegorznowosad.DiscussIT.controller.dto.UserCreateDto;
import pl.grzegorznowosad.DiscussIT.controller.dto.UserDto;
import pl.grzegorznowosad.DiscussIT.model.RoleApplication;
import pl.grzegorznowosad.DiscussIT.model.User;
import pl.grzegorznowosad.DiscussIT.repository.RoleApplicationRepository;
import pl.grzegorznowosad.DiscussIT.repository.UserRepository;
import pl.grzegorznowosad.DiscussIT.service.UserService;
import pl.grzegorznowosad.DiscussIT.service.exception.LoginIncorrect;
import pl.grzegorznowosad.DiscussIT.service.exception.RoleApplicationNotFound;
import pl.grzegorznowosad.DiscussIT.service.exception.UserAlreadyExist;
import pl.grzegorznowosad.DiscussIT.service.mapper.UserDtoMapper;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private RoleApplicationRepository roleApplicationRepository;

    @Mock
    private UserRepository userRepository;

    @Spy
    private UserDtoMapper userDtoMapper = new UserDtoMapper();

    @InjectMocks
    private UserService userService;

    @Captor
    private ArgumentCaptor<User> userArgumentCaptor;

    @Test
    public void createUser_chechIfNewUserHasHashedPassword() throws RoleApplicationNotFound, LoginIncorrect, UserAlreadyExist {
        when(roleApplicationRepository.findByRoleName("USER")).thenReturn(Optional.of(new RoleApplication()));
        userDtoMapper.setRoleApplicationRepository(roleApplicationRepository);


        UserCreateDto userCreateDto = new UserCreateDto("jan", "password123", "Jan", 1980, "mail@mail.com", null);


        User savedUser = new User();
        savedUser.setRole(new RoleApplication());
        when(userRepository.save(userArgumentCaptor.capture())).thenReturn(savedUser);

        userService.createUser(userCreateDto);

        User user = userArgumentCaptor.getValue();

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        boolean isHashCorrect = bCryptPasswordEncoder.matches("password123", user.getPassword());
        assertTrue(isHashCorrect);

    }
}
